<?php

use App\Core\Auth;

function getAllNotifs()
{
    $notifType = Auth::user('role_id');
    $type = ($notifType == 1)?"SA":"U";
    $notifs = DB()->selectLoop("*", "notif", "status = 0 AND notif_type = '$type'")->get();
    $count = 0;
    if (count($notifs) > 0) {
        foreach ($notifs as $notif) {
            if ($type == "SA") {
                $resident = DB()->select("*", "users as r, purok_masterlist as p", "r.purok = p.id AND r.id = '$notif[user_id]'")->get();

                $notifs[$count]['notif'] = "<span onclick='gotorequest(\"".$notif['id']."\",\"".$notif['notif_category']."\",\"".$type."\")'>You have a new request from &nbsp;<b>" . $resident['fullname'] . "</b>.</span>";
            } else {
                if($notif['notif_category'] == 'SR'){
                    $request = DB()->select("*", "request", "id = '$notif[request_id]'")->get();
                    $status = ($request['status'] == 2) ? "Approved" : "Cancelled";
                }else{
                    $request = DB()->select("*", "cash_assistance", "id = '$notif[request_id]'")->get();
                    $status = ($request['status'] == 1) ? "Approved" : "Cancelled";
                }
               
                

                $notifs[$count]['notif'] = "<span onclick='gotorequest(\"".$notif['id']."\",\"".$notif['notif_category']."\",\"".$type."\")'>Your request has been &nbsp;<b>{$status}</b>&nbsp; by the admin.</span>";
            }

            $count++;
        }
    } else {
        $notifs[0]['notif'] = "No New Notifications.";
    }

    return $notifs;
}
function notifCounter(){
    $notifType = getRole(Auth::user('role_id'));
    $notifs = DB()->selectLoop("*", "notif", "status = 0 AND notif_type = '$notifType'")->get();

    $count = count($notifs);

    return $count;
}
function getRole($role_id)
{
    $role = DB()->select("*", "roles", "id = '$role_id'")->get();
    return $role['role'];
}


function countSupplyInTypes($id){
    $count = DB()->select("count(*) as c","supplies","supply_type = '$id'")->get();

    return $count['c'];
}

function getFullname($id){
    $name = DB()->select("fullname", "users", "id = '$id'")->get();

    return $name['fullname'];
}

function getTotalExpired($supply_id, $measurement){
    $date_now = date('Y-m-d');

    $expired = DB()->select("sum(quantity) as exp", "supply_stocks", "supply_id = '$supply_id' AND CONCAT(stock_measure,'',unit_measure) = '$measurement' AND expiry < '$date_now'")->get();

    return $expired['exp']*1;
}

function INVout($supply_id, $measurement){
    $out = DB()->select("sum(d.quantity) as t","request as r, request_detail as d, supply_stocks as ss", "d.supply_id = '$supply_id' AND r.status = '3' AND d.supply_id = ss.supply_id AND d.unit_of_measure = '$measurement'")->get();

    return $out['t']*1;
}

function INVin($supply_id, $unitofmeasure){
    $in = DB()->select("sum(quantity) as t","supply_stocks", "supply_id = '$supply_id' AND CONCAT(stock_measure,'',unit_measure) = '$unitofmeasure'")->get();

    return $in['t']*1;
}
