<?php

/**
 * --------------------------------------------------------------------------
 * Routes
 * --------------------------------------------------------------------------
 * 
 * Here is where you can register routes for your application.
 * Now create something great!
 * 
 */

use App\Core\Routing\Route;

Route::get('/', function () {
    $pageTitle = "Welcome";
    return view('/welcome', compact('pageTitle'));
});

// Route::get('/', function () {
//     redirect('/dashboard');
// });

Route::get('/home', function () {
    redirect('/dashboard');
});

Route::get('/dashboard', ['WelcomeController@home', ['auth']]);
Route::post('/update-notif', ['WelcomeController@updateNotif']);

Route::group(["prefix" => "resident", "middleware" => ["auth"]], function () {
    Route::get('/masterlist', ['ResidentController@index']);
    Route::get('/request', ['ResidentController@request']);
    Route::post('/save', ['ResidentController@store']);
    Route::post('/per-purok', ['ResidentController@residentPerPurok']);
    Route::post('/approve', ['ResidentController@approve']);
    Route::get('/view-details/{id}', ['ResidentController@viewRequest']);
});

Route::group(["prefix" => "purok", "middleware" => ["auth"]], function () {
    Route::get('/masterlist', ['PurokController@index']);
    Route::post('/save', ['PurokController@store']);
    Route::post('/delete', ['PurokController@delete']);
    Route::post('/update', ['PurokController@update']);
});

Route::group(["prefix" => "supply", "middleware" => ["auth"]], function () {
    Route::get('/masterlist', ['SupplyController@index']);
    Route::get('/stocks/{supply}', ['SupplyController@stocks']);
    Route::get('/availability/{type}', ['SupplyController@availability']);

    Route::post('/save-category', ['SupplyController@store']);
    Route::post('/save-per-type', ['SupplyController@savePerType']);
    Route::post('/add-stocks', ['SupplyController@addStocks']);
    Route::post('/stocks-data', ['SupplyController@stocksData']);
    Route::post('/delete-stocks', ['SupplyController@deleteStocks']);
});

Route::group(["prefix" => "sms", "middleware" => ["auth"]], function () {
    Route::get('/', ['SmsController@index']);
    Route::post('/recepients', ['SmsController@recepients']);
    Route::post('/send', ['SmsController@send']);
   /// Route::get('/send', ['SmsController@send']);
});

Route::group(["prefix" => "calendar", "middleware" => ["auth"]], function () {
    Route::get('/', ['CalendarController@index']);

    Route::post('/add-event', ['CalendarController@store']);
    Route::post('/drop-event', ['CalendarController@drop']);
    Route::post('/delete-event', ['CalendarController@delete']);
    Route::post('/update-event', ['CalendarController@update']);
});

Route::group(["prefix" => "cash", "middleware" => ["auth"]], function () {
    Route::get('/', ['CashAssistanceController@index']);
    Route::get('/requests', ['CashAssistanceController@requests']);

    Route::post('/cash-assistance', ['CashAssistanceController@cashAssitanceList']);
    Route::post('/request-new', ['CashAssistanceController@store']);
    Route::post('/approve-request', ['CashAssistanceController@approve']);
    Route::post('/cancel-request', ['CashAssistanceController@cancel']);
});

Route::group(["prefix" => "request", "middleware" => ["auth"]], function () {
    Route::get('/', ['RequestController@index']);
    Route::post('/recepients', ['RequestController@recepients']);
    Route::get('/send', ['RequestController@send']);
    Route::post('/save-head', ['RequestController@saveHead']);
    Route::post('/items-per-ption', ['RequestController@itemsPerOption']);
    Route::post('/save-detail', ['RequestController@saveDetail']);
    Route::post('/detail-list', ['RequestController@detailList']);
    Route::post('/show-measures', ['RequestController@showMeasures']);
    Route::post('/finish-request', ['RequestController@finish']);
    Route::post('/delete-detail', ['RequestController@delete']);
    Route::post('/check-inventory', ['RequestController@inventory']);
    Route::get('/view/{id}', ['RequestController@viewRequest']);
    Route::get('/status-to/{status}/{id}', ['RequestController@updateStatusTo']);
});

Route::group(["prefix" => "report", "middleware" => ["auth"]], function () {
    Route::get('/cash-assistance', ['ReportController@cashAssistance']);
    Route::get('/inventory', ['ReportController@inventory']);
    Route::get('/inventory-supply', ['ReportController@inventorySupply']);

    Route::post('/assistance-report', ['ReportController@assistanceReport']);
    Route::post('/supply-report', ['ReportController@supplyReport']);
});

