</div>
<script>
function gotorequest(id, category, type){
    $.post(base_url+"/update-notif", {
        id: id
    }, function(res){
        if(category  == 'CA'){
            if(type == 'SA'){
                window.location.href = "<?= route('/cash/requests') ?>";
            }else{
                window.location = "<?= route('/cash') ?>";
            }
           
        }else{
            if(type == 'SA'){
                window.location.href = "<?= route('/request') ?>";
            }else{
                window.location = "<?= route('/resident/request') ?>";
            }
        }
    });
}
function alert_response(head, body, icon) {
    Swal.fire(
        head,
        body,
        icon
    ).then((value) => {
        window.location.reload();
    });
}

function alert_response_with_link(head, body, icon, link) {
    Swal.fire(
        head,
        body,
        icon
    ).then((value) => {
        window.location = link;
    });
}

function alert_response_without_reload(head, body, icon) {
    Swal.fire(
        head,
        body,
        icon
    );
}
</script>
</body>

</html>