<?php

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
?>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel='icon' href='<?= public_url('/favicon.ico') ?>' type='image/ico' />
	<title>
		<?= ucfirst($pageTitle) . " | " . App::get('config')['app']['name'] ?>
	</title>

	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

	<link href="<?= public_url('/assets/material/css/material-dashboard.css?v=2.1.2') ?>" rel="stylesheet" />
	<link href="<?= public_url('/assets/material/demo/demo.css') ?>" rel="stylesheet" />
	<link href="<?= public_url('/assets/material/fullcalendar/fullcalendar.min.css') ?>" rel='stylesheet' />
	<link href="<?= public_url('/assets/material/fullcalendar/fullcalendar.print.min.css') ?>" rel='stylesheet' media='print' />
	<link href="<?= public_url('/assets/material/datatables/datatables_bootstrap4.min.css') ?>" rel='stylesheet' />
	<link href="<?= public_url('/assets/material/css/sweetalert2.min.css') ?>" rel='stylesheet' />
	<link href="<?= public_url('/assets/material/css/bootstrap-datetimepicker.min.css') ?>" rel='stylesheet' />
	<link href="<?= public_url('/assets/material/fselect/fselect.min.css') ?>" rel='stylesheet' />

	<script src="<?= public_url('/assets/material/js/core/jquery.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/core/popper.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/core/bootstrap-material-design.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/moment.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/sweetalert2.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/jquery.validate.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/jquery.bootstrap-wizard.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/bootstrap-selectpicker.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/bootstrap-datetimepicker.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/jquery.dataTables.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/bootstrap-tagsinput.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/fullcalendar.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/nouislider.min.js') ?>"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
	<script src="<?= public_url('/assets/material/js/plugins/arrive.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/bootstrap-notify.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/material-dashboard.js?v=2.1.2') ?>" type="text/javascript"></script>
	<script src="<?= public_url('/assets/material/demo/demo.js') ?>"></script>
	<script src="<?= public_url('/assets/material/datatables/jquery_datatables.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/datatables/datatables_bootstrap4.min.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/highcharts1.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/highcharts-more.js') ?>"></script>
	<script src="<?= public_url('/assets/material/js/plugins/exporting.js') ?>"></script>
	<script src="<?= public_url('/assets/material/fselect/fselect.min.js') ?>"></script>

	<script>
		$(document).ready( function(){
			$('[data-toggle="tooltip"]').tooltip();
		});
		const base_url = "<?= App::get('base_url') ?>";

		
	</script>
</head>
</head>

<body>
	<div class="wrapper ">
		<div class="sidebar" data-color="azure" data-background-color="white" data-image="<?= public_url('/assets/material/images/pms-bg2.jpg') ?>">
			<div class="logo">
				<a href="#" class="simple-text logo-normal">
					<img src="<?= public_url('/storage/images/tangub_logo_v2.png') ?>" style="height: 36px;width: 40px;"> TANGUB
				</a>
			</div>
			<div class="sidebar-wrapper">
				<ul class="nav">
					<li class="nav-item">
						<a class="nav-link" href="<?= route('/dashboard') ?>">
							<i class="material-icons">dashboard</i>
							<p>Dashboard</p>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= route('/calendar') ?>">
							<i class="material-icons">perm_contact_calendar</i>
							<p>Calendar Of Events</p>
						</a>
					</li>
					<?php if (getRole(Auth::user('role_id')) == 'SA') { ?>
						
						<li class="nav-item ">
							<a class="nav-link collapsed" data-toggle="collapse" href="#pagesExamples" aria-expanded="true">
								<i class="material-icons">list</i>
								<p> Master Lists
									<b class="caret"></b>
								</p>
							</a>
							<div class="collapse" id="pagesExamples">
								<ul class="nav">
									<li class="nav-item">
										<a class="nav-link" href="<?= route('/purok/masterlist') ?>">
											<i class="material-icons">content_paste</i>
											<p>Purok</p>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="<?= route('/resident/masterlist') ?>">
											<i class="material-icons">content_paste</i>
											<p>Residents</p>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="<?= route('/supply/masterlist') ?>">
											<i class="material-icons">content_paste</i>
											<p>Supplies</p>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= route('/sms') ?>">
								<i class="material-icons">sms</i>
								<p>SMS</p>
							</a>
						</li>
						<li class="nav-item ">
							<a class="nav-link collapsed" data-toggle="collapse" href="#requests" aria-expanded="true">
								<i class="material-icons">list</i>
								<p> Requests
									<b class="caret"></b>
								</p>
							</a>
							<div class="collapse" id="requests">
								<ul class="nav">
									<li class="nav-item">
										<a class="nav-link" href="<?= route('/request') ?>">
											<i class="material-icons">content_paste</i>
											<p>Supplies</p>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="<?= route('/cash/requests') ?>">
											<i class="material-icons">attach_money</i>
											<p>Cash Assistance</p>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= route('/report/inventory-supply') ?>">
								<i class="material-icons">library_books</i>
								<p>Inventory</p>
							</a>
						</li>
						
						<li class="nav-item ">
							<a class="nav-link" data-toggle="collapse" href="#reports" aria-expanded="true">
								<i class="fa fa-bar-chart"></i>
								<p> Reports
									<b class="caret"></b>
								</p>
							</a>
							<div class="collapse" id="reports">
								<ul class="nav">
									<li class="nav-item">
										<a class="nav-link" href="<?= route('/report/inventory') ?>">
											<i class="fa fa-bar-chart"></i>
											<p>Supply</p>
										</a>
									</li>
									<li class="nav-item">
										<a class="nav-link" href="<?= route('/report/cash-assistance') ?>">
											<i class="fa fa-bar-chart"></i>
											<p>Cash Assistance</p>
										</a>
									</li>
								</ul>
							</div>
						</li>
					<?php } ?>
					<?php if (getRole(Auth::user('role_id')) == 'U') { ?>
						<li class="nav-item">
							<a class="nav-link" href="<?= route('/resident/request') ?>">
								<i class="material-icons">content_paste</i>
								<p>My Request</p>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?= route('/cash') ?>">
								<i class="material-icons">attach_money</i>
								<p>Cash Assistance</p>
							</a>
						</li>
					<?php } ?>
					<hr>
					<li class="nav-item" style="display: inline-flex;margin: 0px 0px 0px 37px;">
						<a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="User Profile" href="<?= route('/profile') ?>" style="height: 54px;width: 58px;background: #4fbcd4;">
							<i class="material-icons" style="color: white;">manage_accounts</i>
						</a>
						<a class="nav-link" data-toggle="tooltip" data-placement="bottom" title="Logout" href="<?= route('/logout') ?>" style="height: 54px;width: 58px;background: #4fbcd4;" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
							<i class="material-icons" style="color: white;">logout</i>
						</a>

						<form id="logout-form" action="<?= route('/logout') ?>" method="POST" style="display:none;">
							<?= csrf() ?>
						</form>
					</li>
				</ul>
			</div>
		</div>
		<div class="main-panel">
			<!-- Navbar -->
			<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
				<div class="container-fluid">
					<div class="navbar-wrapper">
						<a class="navbar-brand" href="javascript:;"></a>
					</div>
					<button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="navbar-toggler-icon icon-bar"></span>
						<span class="navbar-toggler-icon icon-bar"></span>
						<span class="navbar-toggler-icon icon-bar"></span>
					</button>
					<div class="collapse navbar-collapse justify-content-end">
						<ul class="navbar-nav">
							<li class="nav-item dropdown">
								<a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">notifications</i>
									<span class="notification"><?=notifCounter()?></span>
								</a>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink" style='width: 450px;'>

									<?php
									foreach (getAllNotifs() as $notif) :
									?>
										<a class='dropdown-item' onclick='' href='#'>
											<span class='fa fa-paper-plane' style='color: #26c6da'> </span>&nbsp;<?= $notif["notif"] ?>
										</a>
									<?php endforeach; ?>
									<span class='divider'></span>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- End Navbar -->
			<div class="content">
				<div class="container-fluid">