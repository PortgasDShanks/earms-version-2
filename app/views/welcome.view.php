<?php

use App\Core\App;
?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='icon' href='<?= public_url('/favicon.ico') ?>' type='image/ico' />
    <title>
        <?= ucfirst($pageTitle) . " | " . App::get('config')['app']['name'] ?>
    </title>

    <link rel="stylesheet" href="<?= public_url('/assets/sprnva/css/bootstrap.min.css') ?>">

    <style>
        @font-face {
            font-family: Nunito;
            src: url("<?= public_url('/assets/sprnva/fonts/Nunito-Regular.ttf') ?>");
        }

        body {
            font-weight: 300;
            font-family: Nunito;
            color: #26425f;
            background: #eef1f4;
        }

        .bg-light {
            background-color: #ffffff !important;
        }

        .card {
            box-shadow: 0px;
            margin-bottom: 0rem;
            border-radius: 0rem !important;
            border: 1px solid rgba(0, 0, 0, 0.2);
        }

        .wlcm-link {
            text-decoration: underline !important;
            color: inherit;
        }

        .link-green {
            color: #00551f !important;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
        }

        .table-bordered {
            border: 1px solid #ccc;
        }

        .table td, .table th {
            padding: 1.50rem;
        }
    </style>

    <script src="<?= public_url('/assets/sprnva/js/jquery-3.6.0.min.js') ?>"></script>
    <script src="<?= public_url('/assets/sprnva/js/popper.min.js') ?>"></script>
    <script src="<?= public_url('/assets/sprnva/js/bootstrap.min.js') ?>"></script>

    <?php
    // this will auto include filepond css/js when adding filepond in public/assets
    if (file_exists('public/assets/filepond')) {
        require_once 'public/assets/filepond/filepond.php';
    }
    ?>

    <script>
        const base_url = "<?= App::get('base_url') ?>";
    </script>
</head>
</head>

<body>
    <div class="container mt-1">
        <div class="row">
            <div class="col-12 d-flex flex-row justify-content-end">
                <ul class="navbar-nav flex-row ml-md-auto">
                    <?php if (fortified()) { ?>
                        <li class="nav-item">
                            <a class="nav-link link-green" href="<?= route('/login') ?>">Login</a>
                        </li>
                        <li class="nav-item pl-2 pr-2">
                            <span class="nav-link text-muted">|</span>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link link-green" href="<?= route('/register') ?>">Register</a>
                        </li>
                    <?php }else{ ?>
                        <li class="nav-item">
                            <a class="nav-link link-green">&nbsp;</a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="col-12">
                <div class="d-flex flex-row align-items-center">
                    <img src="<?= public_url('/storage/images/tangub_logo_v2.png') ?>" alt="sprnva-logo" style="width: 65px; height: 65px;">
                    <h1 class="pl-2 mb-0" style="color: #00551f;font-weight: 600;">BRGY. TANGUB Emergency Assistance</h1>
                </div>
                <div class="card mt-3" style="border-radius: 0.5rem !important;">
                    <!-- <h5 class="card-header">Welcome to your Sprnva application!</h5> -->
                    <div class="card-body">
                        <!-- <p class="card-text">Sprnva will provide you experience and expand your vision for a better understanding of the basics. We'll help you take your first steps as a web developer or give you a boost as you take your expertise to the next level. Featuring Model-view-controller software design pattern, debugging, secure and organized routing, expressive database builder and more.</p> -->
                    </div>
                    <div class="col-12" style="padding: 0px;">
                        <div class="row p-2">
                           
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title" style='text-align: center;text-decoration: underline'>Emergency Numbers</h5>
                                        <table class='table table-hover'>
                                            <thead>
                                                <tr>
                                                    <th>Department</th>
                                                    <th>Contact #</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Disaster Risk Reduction and management Office</td>
                                                    <td>432-3879 / 709-1633</td>
                                                </tr>
                                                <tr>
                                                    <td>Amity</td>
                                                    <td>433-3244</td>
                                                </tr>
                                                <tr>
                                                    <td>Chamber</td>
                                                    <td>432-0111</td>
                                                </tr>
                                                <tr>
                                                    <td>Red Cross</td>
                                                    <td>435-0324</td>
                                                </tr>
                                                <tr>
                                                    <td>Bureau Of Fire Protection </td>
                                                    <td>434-5022/23 & 09213417002</td>
                                                </tr>
                                                <tr>
                                                    <td>Bacolod City Police Office Headquarters </td>
                                                    <td>431-2077</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title" style='text-align: center;text-decoration: underline'>Brgy. Personnel Information</h5>
                                        <table class='table table-hover'>
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Address</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Sheila Paulino</td>
                                                    <td>Purok Paghidaet Barangay Tangub Bacolod City</td>
                                                </tr>

                                                <tr>
                                                    <td>Ronel Cruz</td>
                                                    <td>Purok Paghidaet Barangay Tangub Bacolod City</td>
                                                </tr>

                                                <tr>
                                                    <td>Pamela Jalandoni</td>
                                                    <td>Purok Crossing 8 Barangay Tangub Bacolod City</td>
                                                </tr>

                                                <tr>
                                                    <td>Mary Grace Cabalhin</td>
                                                    <td>Purok Kapawa Barangay Tangub Bacolod City</td>
                                                </tr>

                                                <tr>
                                                    <td>Marivic Belvis</td>
                                                    <td>Purok Mahigugma-on Barangay Tangub Bacolod City</td>
                                                </tr>

                                                <tr>
                                                    <td>Richard Dela Cruz</td>
                                                    <td>Purok Greenfield Barangay Tangub Bacolod City</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-12" style="display: flex;flex-direction: row;justify-content: space-between;margin-top: 2px;padding: 0px;">
                    <div>
                         <p class="card-text pt-1 text-muted"></p>
                    </div>
                    <div>
                        <div class="text-sm mb-3" style="font-size: 14px;">
                            <!-- Sprnva v<?= appversion() ?> (PHP v<?= phpversion() ?>) -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>