<form method="POST" action="<?= route('/supply/save-category') ?>" id="add_supply">
    <div class="modal fade" id="supplyCategory" tabindex="-1" role="dialog" aria-labelledby="supplyCategoryLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="supplyCategoryLabel"><span class='fa fa-check-circle'></span> Add Category to Master List.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6 id="register-response" style="margin-bottom: 30px;"></h6>
                    <div class="md-form">
                        <input type="text" autocomplete='off' id="category" name="category" class="form-control" required>
                        <label for="category">Category <span style="color:red">*</span></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Add Category</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>