<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <?= alert_msg() ?>
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title"><?= $type ?></h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary pull-right' onclick='addsupplypertype("<?= $type_id ?>")' id='supplyCategoryBtn'> Add Data </button>
                        <button class='btn btn-sm btn-primary pull-right' onclick='window.location="<?=route("/supply/masterlist")?>"' id='supplyCategoryBtn'> Back to Categories </button>
                    </div>
                    <div class='col-sm-12'>
                        <div class="table-responsive">
                            <table class="table table-hover" id='master_list'>
                                <thead class=" text-primary">
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Unit of Measures</th>
                                    <th>Expiry Date</th>
                                    <th>Date Added</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    foreach ($supplies as $supply) {
                                    ?>
                                        <tr>
                                            <td><?= $count++; ?></td>
                                            
                                            <td><?= $supply['supply_name'] ?></td>
                                            <td><?=( $supply['measure'] == 0)?"<span style='color: red'>N/A</span>":"<span style='color: green'>YES</span>"?></td>
                                            <td><?=($supply['expiry'] == 0)?"<span style='color: red'>N/A</span>":"<span style='color: green'>YES</span>" ?></td>
                                            <td><?= date('F d, Y', strtotime($supply['date_added'])) ?></td>
                                            <td>
<!--                                                
                                                <i  data-toggle="tooltip" data-placement="bottom" title="View Stocks" style='cursor:pointer' class='material-icons text-success' onclick="stocksCheck(<?=$supply['id']?>)">visibility</i> -->

                                                <i data-toggle="tooltip" data-placement="bottom" title="View Stocks" style='cursor:pointer;background-color: #0cb4c9;padding: 5px;border-radius: 17px;' class='material-icons text-white btn-round' onclick="stocksCheck(<?=$supply['id']?>)">visibility</i>

                                                <i data-toggle="tooltip" data-placement="bottom" title="Delete Item" style='cursor:pointer;background-color: #0cb4c9;padding: 5px;border-radius: 17px;' class='material-icons text-white btn-round' onclick="deleteStocks(<?=$supply['id']?>)">delete_forever</i>

                                                <!-- <i data-toggle="tooltip" data-placement="bottom" title="Delete Item" style='cursor:pointer' class='material-icons text-danger'>delete_forever</i> -->
                                               
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__ . '/add-supply-modal.php'; ?>

<script>
    function addsupplypertype(type) {
        $("#supply").modal();
    }

    $(document).ready(function() {
        $("#master_list").DataTable();
    });

    function stocksCheck(id){
        var routes = "<?=route('/supply/stocks')?>";
        window.location = routes + '/' +id;
    }

    function deleteStocks(id){
        $.post(base_url+"/supply/delete-stocks", {
            id: id
        }, function(res){
            alert_response("All Good!", "Supply Successfully Deleted!", "success");
        })
    }
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>