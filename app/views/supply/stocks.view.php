<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <?= alert_msg() ?>
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title"><?= $supply_name ?></h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary pull-right' onclick='window.location="<?=route("/supply/availability", $type)?>"' id='supplyCategoryBtn'> Back to Supply List </button>
                    </div>
                    <div class='col-sm-4'>
                        <div class='card'>
                            <div class='card-header'>
                                <h4>Add Stocks</h4>
                            </div>
                            <div class='card-body'>
                                <div class="md-form" style='<?=($measure == 1)?"":"display:none"?>'>
                                    <input type="number" autocomplete='off' id="unitofmeasure" name="unitofmeasure" class="form-control" required>
                                    <label for="unitofmeasure">Unit of Measure <span style="color:red">*</span></label>
                                </div>
                                    <div class="form-check form-check-radio form-check-inline" style='<?=($measure == 1)?"":"display:none"?>'>
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio" value="ml"> ml
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-radio form-check-inline" style='<?=($measure == 1)?"":"display:none"?>'>
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio" value="mg"> mg
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="form-check form-check-radio form-check-inline" style='<?=($measure == 1)?"":"display:none"?>'>
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio" value="g"> g
                                            <span class="circle">
                                                <span class="check"></span>
                                            </span>
                                        </label>
                                    </div>
                                <div class="md-form">
                                    <input type="text" autocomplete='off' id="quantity" name="quantity" class="form-control" required>
                                    <label for="quantity">Quantity <span style="color:red">*</span></label>
                                </div>  
                                <div class="md-form" style='<?=($expiry == 1)?"":"display:none"?>'>
                                    <input type="date" autocomplete='off' id="expiryDate" name="expiryDate" class="form-control" required>
                                    <label for="expiryDate">Expiry Date <span style="color:red">*</span></label>
                                </div>
                                <div class="md-form">
                                    <textarea name="description" id="description" rows="2" class='form-control'></textarea>
                                    <label for="description">Description <span style="color:red">*</span></label>
                                </div>
                            </div>
                            <div class='card-footer'>
                                <button onclick="saveStocks()" id='saveBtn' class='btn btn-sm btn-round btn-primary btn-block'><span class='fa fa-plus-circle'></span> ADD</button>
                            </div>
                        </div>
                    </div>
                    <div class='col-sm-8'>
                        <div class='card'>
                            <div class='card-header'>
                                <h4><?= $supply_name ?> Stocks</h4>
                            </div>
                            <div class='card-body'>
                            <div class="table-responsive col-sm-12" style='margin-top: 20px'>
                                <table class="table table-hover table-borderless" id='stocks_table'>
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Unit of Measure</th>
                                            <th>Quantity</th>
                                            <th>Expiry Date</th>
                                            <th>Date Added</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                            <div class='card-footer'>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        supplyStocks_data(<?=$supply?>);
    });

    function saveStocks(){
        if (!$("input[name='inlineRadioOptions']:checked").val()) {
            var res = '';
        } else {
            var res = $("input[name='inlineRadioOptions']:checked").val();
        } 
        var unitofmeasure = ($("#unitofmeasure").val() == '' || $("#unitofmeasure").val() == 0)?0:$("#unitofmeasure").val();
        var quantity = $("#quantity").val();
        var expiryDate = $("#expiryDate").val();
        var description = $("#description").val();
        var supply_id = '<?=$supply?>';

        $("#saveBtn").prop("disabled", true);
        $("#saveBtn").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        $.post(base_url+"/supply/add-stocks", {
            res: res,
            unitofmeasure: unitofmeasure,
            quantity: quantity,
            expiryDate: expiryDate,
            description: description,
            supply_id: supply_id
        }, function(data){
            if(data > 0){
                alert_response_without_reload("All Good!", "Stock Successfully Added", "success");
            }else{
                alert_response_without_reload("Oppss!", "There was an error encountered", "error");
            }
            supplyStocks_data(supply_id);
        });
   
    }
    function supplyStocks_data(supply) {
        $("#stocks_table").DataTable().destroy();
        $('#stocks_table').dataTable({
            "processing": true,
            "ajax": {
                "url": base_url + "/supply/stocks-data",
                "dataSrc": "data",
                "data": {
                    supply: supply
                },
                "type": "POST"
            },
            "columns": [{
                    "data": "count"
                },
                {
                    "data": "unit_of_measure"
                },
                {
                    "data": "quantity"
                },
                {
                    "data": "expiry"
                },
                {
                    "data": "date_added"
                }
            ]
        });
    }
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>