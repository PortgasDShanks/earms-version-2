<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <?= alert_msg() ?>
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Supplies Master List</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target='#supplyCategory' id='supplyCategoryBtn'> Add Supply Category </button>
                    </div>
                    <?php
                    foreach ($supplTypes as $supplType) {
                    ?>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-stats" style="background: #00000005;">
                                <div class="card-header card-header-info card-header-icon" style="text-align: left;">
                                    <div class="card-icon">
                                        <h2><?= (!empty(countSupplyInTypes($supplType['id']))) ? countSupplyInTypes($supplType['id']) : 0; ?></h2>
                                    </div>
                                    <h3 class="card-title"><?= $supplType['type_name'] ?></h3>
                                </div>
                                <div class="card-footer" style="justify-content: center;">
                                    <div class="stats">
                                        <i class="material-icons text-info">remove_red_eye</i>
                                        <a href="<?= route("/supply/availability/{$supplType['id']}") ?>">Check Availability</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__ . '/add-supply-modal-category.php'; ?>

<script>
    $(document).ready(function() {
        $("#master_list").DataTable();
    });
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>