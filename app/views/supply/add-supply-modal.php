<form method="POST" action="<?= route('/supply/save-per-type') ?>" id="add_supply_per_type">
    <div class="modal fade" id="supply" tabindex="-1" role="dialog" aria-labelledby="supplyLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="supplyLabel"><span class='fa fa-plus-circle'></span> Add Supply as <?= $type ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6 id="register-response" style="margin-bottom: 30px;"></h6>
                    <div class="md-form">
                        <input type="text" autocomplete='off' id="supplyname" name="supplyname" class="form-control" required>
                        <input type="hidden" autocomplete='off' id="supplytype" name="supplytype" value='<?= $type_id ?>' class="form-control">
                        <label for="supplyname">Name <span style="color:red">*</span></label>
                    </div>
                  
                    <div class="md-form" style='margin-top: 20px;'>

                        <input type="checkbox" id="uom" name="uom"> Need Unit of Measure ?
                      
                    </div>
                    <div class="md-form">

                        <input type="checkbox" id="expDate" name="expDate"> Have Expiry Date ?
                      
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Add Category</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>