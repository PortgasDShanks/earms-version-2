<?php

use App\Core\Auth;
use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Send Requests</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary pull-right' onclick="window.location='<?= route('/resident/request') ?>'" id='addpurok_btn'> Back </button>
                    </div>
                    <div class='col-sm-6'>
                        <input type='text' class='form-control' disabled value='<?= Auth::user('fullname') ?>'>
                        <label>Requested By</label>
                    </div>
                    <div class='col-sm-6'>
                        <input type='date' class='form-control' id='requestDate'>
                        <label>Request Date</label>
                    </div>
                    <div class='col-sm-12'>
                        <textarea class='form-control' rows='3' style='resize: none' id='remarks'></textarea>
                        <label>Please add note to your request</label>
                    </div>
                    <div class='col-sm-12'>
                        <button class=' btn btn-sm btn-primary pull-right' onclick='addHeaderRequest()'><span class='fa fa-plus-circle'></span> Add Header</button>
                    </div>
                </div>

                <div class='col-sm-12' style='border: 0.1px solid #dfdfdf;margin-top:40px;'></div>
                <div class='col-sm-12' id='details' style='display: none'>
                    <input type='hidden' id='headerID'>
                    <div class='row'>
                        <div class='col-sm-12'>
                            <label style='font-size: 20px;'>&mdash; Choose an option you want to request &mdash; </label>
                        </div>

                        <?php
                        foreach ($supplyTypes as $supplType) {
                        ?>
                            <div class="col-lg-4 col-md-6 col-sm-6" style='cursor: pointer' onclick='requestOptions("<?= $supplType["id"] ?>")'>
                                <div class="card card-stats" style="background: #0000001a;">
                                    <div class="card-header card-header-info card-header-icon" style="text-align: left;">
                                        <div class="card-icon">
                                            <h2><?= (!empty(countSupplyInTypes($supplType['id']))) ? countSupplyInTypes($supplType['id']) : 0; ?></h2>
                                        </div>
                                        <h3 class="card-title"><?= $supplType['type_name'] ?></h3>
                                    </div>
                                    <div class="card-footer" style="justify-content: center;">
                                        <div class="stats">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class='col-md-12' id='itemsCnt' style='display: none'>
                            <div class='row'> 
                                <div class='col-sm-12'>
                                    <h5>Remaining Quantity: <span id='qunttyvalue'></span></h5>
                                </div>
                                <div class='col-lg-6 col-md-6 col-sm-6'>
                                    <select class='form-control' id='option_val' onchange='getUnitofmeasure()'>
                                        
                                    </select><label>Which Item would you want to be requested?</label>
                                </div>

                                <div class='col-lg-6 col-md-6 col-sm-6' style='margin-top: 5px'>
                                    <select class='form-control' id='unitMeasurement' onchange='checkInventory()'>
                                        
                                    </select>
                                    <label>Unit of Measure</label>
                                </div>

                                <div class='col-lg-6 col-md-6 col-sm-6' style='margin-top: 5px'>
                                    <input type='number' id='qntty_val' class='form-control'>
                                    <label>Quantity</label>
                                </div>
                                
                                
                                <div class='col-sm-12'>
                                    <button id='addItemBtn' class='btn btn-sm btn-primary pull-right' onclick='addDetailRequest()'><span class='fa fa-plus-circle'></span> Add Entry</button>
                                </div>
                            </div>
                        </div>
                        <input type='hidden' id='optionSelected'>
                        <div class='col-sm-12' style='border: 0.1px solid #dfdfdf;margin-top:40px;'></div>
                        <div class='col-sm-12' style='display:none' id='table_details'>
                            <button class='btn btn-primary btn-sm pull-right' onclick='finishRequest()'><span class='fa fa-check-circle'></span> Finish Request</button>
                            <div class="table-responsive">
                                <table class="table table-hover" id='request_detail_list'>
                                    <thead class=" text-primary">
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Unit of Measure</th>
                                        <th>Quantity</th>
                                        <th>Category</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    function addHeaderRequest() {
        var requestDate = $("#requestDate").val();
        var remarks = $("#remarks").val();
        $.post(base_url + "/request/save-head", {
            requestDate: requestDate,
            remarks: remarks
        }, function(data) {
            $("#headerID").val(data);
            $("#details").css("display", "block");
        });

    }

    function requestOptions(option) {
        $("#optionSelected").val(option);
        $("#items").html("<span class='fa fa-spin fa-spinner'></span>").css("text-align", "center").css("font-size", "50px");
        $.post(base_url + "/request/items-per-ption", {
            option: option
        }, function(data) {
            $("#itemsCnt").css('display','block');
            if(data == 0){
                $("#unitMeasurement").prop("disabled", true);
                $("#qntty_val").prop("disabled", true);
                $("#addItemBtn").prop("disabled", true);
            }else{
                $("#option_val").html(data);

            $("#unitMeasurement").prop("disabled", false);
                $("#qntty_val").prop("disabled", false);
                $("#addItemBtn").prop("disabled", false);
            }
            
        });
    }

    function addDetailRequest() {
        var qunttyvalue = $("#qunttyvalue").text();
        var qntty = $("#qntty_val").val();
        var option_val = $("#option_val").val();
        var optionSelected = $("#optionSelected").val();
        var unitMeasurement = $("#unitMeasurement").val();
        var headerID = $("#headerID").val();
        if(qntty < parseInt(qunttyvalue)){
        

            $.post(base_url + "/request/save-detail", {
                option_val: option_val,
                qntty: qntty,
                optionSelected: optionSelected,
                headerID: headerID,
                unitMeasurement: unitMeasurement
            }, function(data) {
                getDetailsData(headerID);
                $("#table_details").css("display", "block");
            });
        }else{
            swal.fire("Unable to add item, Insufficient Stocks");
            $("#qntty_val").val("");
        }
        
    }

    function getDetailsData(headerID) {
        $("#request_detail_list").DataTable().destroy();
        $('#request_detail_list').dataTable({
            "processing": true,
            "ajax": {
                "url": base_url + "/request/detail-list",
                "dataSrc": "data",
                "data": {
                    headerID: headerID
                },
                "type": "POST"
            },
            "columns": [{
                    "data": "count"
                },
                {
                    "data": "supply_name"
                },
                {
                    "data": "unitMeasure"
                },
                {
                    "data": "quantity"
                },
                {
                    "data": "category"
                },
                {
                    "data": "action"
                }

            ]
        });
    }
    function finishRequest() {
        var headerID = $("#headerID").val();
        $.post(base_url + "/request/finish-request", {
            headerID: headerID
        }, function(data) {
            if (data > 0) {
                alert_response_with_link("All Good!", "Request Successfully Sent!", "success", "<?= route('/resident/request') ?>")
            } else {
                alert_response("Oppss!", "There was an error encountered", "error");
            }
        });
    }

    function checkInventory(){
        var option_val = $("#option_val").val();
        var unitMeasurement= $("#unitMeasurement").val();
        $.post(base_url+"/request/check-inventory", {
            option_val: option_val,
            unitMeasurement: unitMeasurement
        }, function(res){
            $("#qunttyvalue").text(res);

            if(res > 0){
                $("#qunttyvalue").css("color",'black');
            }else{
                $("#qunttyvalue").css("color",'red');
            }
        });
    }

    function getUnitofmeasure(){
        
    var option_val = $("#option_val").val();
    
        $.post(base_url+"/request/show-measures", {
            option_val: option_val
        }, function(res){
        
        $("#unitMeasurement").html(res);
        });
    }
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>