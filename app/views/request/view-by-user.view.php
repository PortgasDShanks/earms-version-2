<?php

use App\Core\Request;
use App\Core\Auth;
require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">View Requests</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
     
                    <input type='hidden' id='headerID' value='<?= $id ?>'>
                    <div class='col-sm-6' style='margin-top:10px;'>
                        <label>STATUS:  <?=($requests[0]['status'] == 0)?"<span class='alert alert-warning'>SAVED</span>":(($requests[0]['status'] == 1)?"<span class='alert alert-info'>SUBMITTED</span>":(($requests[0]['status'] == 2)?"<span class='alert alert-success'>APPROVED</span>":(($requests[0]['status'] == 3)?"<span class='alert alert-primary'>COMPLETED</span>":"<span class='alert alert-danger'>CANCELLED</span>")))?></label>
                    </div>
                    <div class='col-sm-6' style='margin-top:10px;'>
                        <button class='btn btn-sm btn-primary pull-right' onclick="window.location='<?= route('/resident/request') ?>'" id='addpurok_btn'> Back </button>
                    </div>
                    <div class='col-sm-6'>
                        <input type='text' class='form-control' disabled value='<?=$requests[0]['fullname']?>'>
                        <label>Requested By</label>
                    </div>
                    <div class='col-sm-6'>
                        <input type='text' class='form-control' disabled id='requestDate' value="<?=date("F d, Y", strtotime($requests[0]['request_date']))?>">
                        <label>Request Date</label>
                    </div>
                    <div class='col-sm-12'>
                        <textarea class='form-control' rows='3' style='resize: none' disabled id='remarks'><?=$requests[0]['remarks']?></textarea>
                        <label>Please add note to your request</label>
                    </div>
                    <!-- <div class='col-sm-12' style=''>
                        <button class=' btn btn-sm btn-primary pull-right' id='updateHeader' onclick='updateHeaderRequest()'><span class='fa fa-plus-circle'></span> Update Header</button>
                    </div> -->
                </div>

                <div class='col-sm-12' style='border: 0.1px solid #dfdfdf;margin-top:40px;'> </div>
                <div class='row' style='<?=($requests[0]['status'] == 0)?"":"display:none"?>'>
                    <div class='col-sm-12'>
                        <label style='font-size: 20px;'>&mdash; Choose an option you want to request &mdash; </label>
                    </div>

                    <?php
                    foreach ($supplyTypes as $supplType) {
                    ?>
                        <div class="col-lg-4 col-md-6 col-sm-6" style='cursor: pointer' onclick='requestOptions("<?= $supplType["id"] ?>")'>
                            <div class="card card-stats" style="background: #0000001a;">
                                <div class="card-header card-header-info card-header-icon" style="text-align: left;">
                                    <div class="card-icon">
                                        <h2><?= (!empty(countSupplyInTypes($supplType['id']))) ? countSupplyInTypes($supplType['id']) : 0; ?></h2>
                                    </div>
                                    <h3 class="card-title"><?= $supplType['type_name'] ?></h3>
                                </div>
                                <div class="card-footer" style="justify-content: center;">
                                    <div class="stats">

                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    
                </div>
                 
                <div class='col-md-12' id='itemsCnt' style='display: none'>
                    <div class='row'> 
                        <div class='col-sm-12'>
                            <h5>Remaining Quantity: <span id='qunttyvalue'></span></h5>
                        </div>
                        <div class='col-lg-6 col-md-6 col-sm-6'>
                            <select class='form-control' id='option_val' onchange='getUnitofmeasure()'>
                                
                            </select><label>Which Item would you want to be requested?</label>
                        </div>

                        <div class='col-lg-6 col-md-6 col-sm-6' style='margin-top: 5px'>
                            <select class='form-control' id='unitMeasurement' onchange='checkInventory()'>
                                
                            </select>
                            <label>Unit of Measure</label>
                        </div>

                        <div class='col-lg-6 col-md-6 col-sm-6' style='margin-top: 5px'>
                            <input type='number' id='qntty_val' class='form-control'>
                            <label>Quantity</label>
                        </div>
                        
                        
                        <div class='col-sm-12'>
                            <button id='addItemBtn' class='btn btn-sm btn-primary pull-right' onclick='addDetailRequest()'><span class='fa fa-plus-circle'></span> Add Entry</button>
                        </div>
                    </div>
                </div>
              
                <div class='col-sm-12' id='details'>
                    <div class='row'>
                        <input type='hidden' id='optionSelected'>
                        <div class='col-sm-12' style='border: 0.1px solid #dfdfdf;margin-top:40px;'></div>
                        <div class='col-sm-12' id='table_details'>
                            <button class='btn btn-primary btn-sm pull-right' style='<?=($requests[0]['status'] == 0)?"":"display:none"?>' onclick='finishRequest()'><span class='fa fa-check-circle'></span> Finish Request</button>
                            <div class="table-responsive">
                                <table class="table table-hover" id='request_detail_list'>
                                    <thead class=" text-primary">
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Unit of Measure</th>
                                        <th>Quantity</th>
                                        <th>Category</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
$(document).ready( function(){
    getDetailsData(<?=$id?>);
});

function addDetailRequest() {
    var qunttyvalue = $("#qunttyvalue").text();
    var qntty = $("#qntty_val").val();
    var option_val = $("#option_val").val();
    var optionSelected = $("#optionSelected").val();
    var unitMeasurement = $("#unitMeasurement").val();
    var headerID = '<?=$id?>';
    if(qntty < parseInt(qunttyvalue)){
      

        $.post(base_url + "/request/save-detail", {
            option_val: option_val,
            qntty: qntty,
            optionSelected: optionSelected,
            headerID: headerID,
            unitMeasurement: unitMeasurement
        }, function(data) {
            getDetailsData(headerID);
            $("#table_details").css("display", "block");
        });
    }else{
        swal.fire("Unable to add item, Insufficient Stocks");
        $("#qntty_val").val("");
    }
    
}
function getDetailsData(headerID) {
    $("#request_detail_list").DataTable().destroy();
    $('#request_detail_list').dataTable({
        "processing": true,
        "ajax": {
            "url": base_url + "/request/detail-list",
            "dataSrc": "data",
            "data": {
                headerID: headerID
            },
            "type": "POST"
        },
        "columns": [{
                "data": "count"
            },
            {
                "data": "supply_name"
            },
            {
                "data": "unitMeasure"
            },
            {
                "data": "quantity"
            },
            {
                "data": "category"
            },
            {
                "data": "action"
            }

        ]
    });
}

function requestOptions(option) {
    $("#optionSelected").val(option);
    $("#items").html("<span class='fa fa-spin fa-spinner'></span>").css("text-align", "center").css("font-size", "50px");
    $.post(base_url + "/request/items-per-ption", {
        option: option
    }, function(data) {
        $("#itemsCnt").css('display','block');
        if(data == 0){
            $("#unitMeasurement").prop("disabled", true);
            $("#qntty_val").prop("disabled", true);
            $("#addItemBtn").prop("disabled", true);
        }else{
            $("#option_val").html(data);

           $("#unitMeasurement").prop("disabled", false);
            $("#qntty_val").prop("disabled", false);
            $("#addItemBtn").prop("disabled", false);
        }
        
    });
}

function delete_item_from_request(id){
    $.post(base_url+"/request/delete-detail", {
        id: id
    }, function(res){
        if(res > 0){
            alert_response_without_reload("All Good!", "Item Successfully deleted", "success");
        }else{
            alert_response_without_reload("Oppss!", "There was an error encountered", "error");
        }

        getDetailsData(<?=$id?>);
    });
}
function finishRequest() {
    var headerID = '<?=$id?>';
    $.post(base_url + "/request/finish-request", {
        headerID: headerID
    }, function(data) {
        if (data > 0) {
            alert_response_with_link("All Good!", "Request Successfully Sent!", "success", "<?= route('/resident/request') ?>")
        } else {
            alert_response("Oppss!", "There was an error encountered", "error");
        }
    });
}

function checkInventory(){
    var option_val = $("#option_val").val();
    var unitMeasurement= $("#unitMeasurement").val();
    $.post(base_url+"/request/check-inventory", {
        option_val: option_val,
        unitMeasurement: unitMeasurement
    }, function(res){
        $("#qunttyvalue").text(res);

        if(res > 0){
            $("#qunttyvalue").css("color",'black');
        }else{
            $("#qunttyvalue").css("color",'red');
        }
    });
}

function getUnitofmeasure(){
    
   var option_val = $("#option_val").val();
   
    $.post(base_url+"/request/show-measures", {
        option_val: option_val
    }, function(res){
       
       $("#unitMeasurement").html(res);
    });
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>