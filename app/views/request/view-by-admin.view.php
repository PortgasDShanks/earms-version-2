<?php

use App\Core\Request;
use App\Core\Auth;
require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">View Requests</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-6' style='margin-top:10px;'>
                        <label>STATUS:  <?=($requests['status'] == 0)?"<span class='alert alert-warning'>SAVED</span>":(($requests['status'] == 1)?"<span class='alert alert-info'>SUBMITTED</span>":(($requests['status'] == 2)?"<span class='alert alert-success'>APPROVED</span>":(($requests['status'] == 3)?"<span class='alert alert-primary'>COMPLETED</span>":"<span class='alert alert-danger'>CANCELLED</span>")))?></label>
                    </div>
                    <div class='col-sm-6' style='margin-top:10px;'>
                        
                        <i data-toggle="tooltip" data-placement="bottom" title="Back to Requests" onclick='window.location="<?= route("/request") ?>"' style='background-color: #0cb4c9;padding: 5px;border-radius: 17px;cursor:pointer;padding:5px;margin-left: 10px;' class="material-icons text-white  pull-right">arrow_back</i>

                        
                        
                        <i data-toggle="tooltip" data-placement="bottom" title="Approve Request" onclick='window.location="<?= route("/request/status-to/approve", $id) ?>"' style='background-color: #0cb4c9;padding: 5px;border-radius: 17px;margin-left: 10px;cursor:pointer;padding:5px;<?= $approve ?>' class="material-icons text-white  pull-right">thumb_up</i>

                        <i data-toggle="tooltip" data-placement="bottom" title="Complete Request" onclick='window.location="<?= route("/request/status-to/finish", $id) ?>"' style='background-color: #0cb4c9;padding: 5px;border-radius: 17px;margin-left: 10px;cursor:pointer;padding:5px;<?= $complated ?>' class='material-icons text-white  pull-right'>done_all</i>

                        <i data-toggle="tooltip" data-placement="bottom" title="Cancel Request" onclick='window.location="<?= route("/request/status-to/cancel", $id) ?>"' style='background-color: #0cb4c9;padding: 5px;border-radius: 17px;margin-left: 10px;cursor:pointer;padding:5px;<?= $cancelled ?>' class='material-icons text-white  pull-right'>do_not_disturb</i>

        
                        <!-- <button class='btn btn-sm btn-info pull-right' style='' onclick="requestResponse('approve')" id='addpurok_btn'> Approve </button>
                        <button class='btn btn-sm btn-success pull-right' style='' onclick="requestResponse('finish')" id='addpurok_btn'> Finish </button>
                        <button class='btn btn-sm btn-danger pull-right' style='' onclick="requestResponse('cancel')'" id='addpurok_btn'> Cancel </button> -->
                   
                    </div>
                    <div class='col-sm-6'>
                        <input type='text' class='form-control' disabled value='<?=$requests['fullname']?>'>
                        <label>Requested By</label>
                    </div>
                    <div class='col-sm-6'>
                        <input type='text' class='form-control' disabled id='requestDate' value="<?=date("F d, Y", strtotime($requests['request_date']))?>">
                        <label>Request Date</label>
                    </div>
                    <div class='col-sm-12'>
                        <textarea class='form-control' rows='3' style='resize: none' disabled id='remarks'><?=$requests['remarks']?></textarea>
                        <label>Please add note to your request</label>
                    </div>
                </div>

                <div class='col-sm-12' style='border: 0.1px solid #dfdfdf;margin-top:40px;'> </div>
                <div class='col-sm-12' id='details'>
                    <div class='row'>
                        <input type='hidden' id='optionSelected'>
                        <div class='col-sm-12' style='border: 0.1px solid #dfdfdf;margin-top:40px;'></div>
                        <div class='col-sm-12' id='table_details'>
                         
                            <div class="table-responsive">
                                <table class="table table-hover" id='request_detail_list'>
                                    <thead class=" text-primary">
                                        <th>#</th>
                                        <th>Item</th>
                                        <th>Unit of Measure</th>
                                        <th>Quantity</th>
                                        <th>Category</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
$(document).ready( function(){
    getDetailsData(<?=$id?>);
});

function getDetailsData(headerID) {
    $("#request_detail_list").DataTable().destroy();
    $('#request_detail_list').dataTable({
        "processing": true,
        "ajax": {
            "url": base_url + "/request/detail-list",
            "dataSrc": "data",
            "data": {
                headerID: headerID
            },
            "type": "POST"
        },
        "columns": [{
                "data": "count"
            },
            {
                "data": "supply_name"
            },
            {
                "data": "unitMeasure"
            },
            {
                "data": "quantity"
            },
            {
                "data": "category"
            },
            {
                "data": "action"
            }

        ]
    });
}

</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>