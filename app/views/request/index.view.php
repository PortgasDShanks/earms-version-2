<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <?= alert_msg() ?>
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Requests</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-12'>
                        <div class="table-responsive">
                            <table class="table table-hover" id='residents_request' style='width: 100%'>
                                <thead class=" text-primary">
                                    <!-- <th></th> -->
                                    <th>#</th>
                                    <th>Request By</th>
                                    <th>Purok</th>
                                    <th>Request Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    foreach ($requests as $request) {
                                       
                                        $approve = ($request['status'] == 1) ? "" : "display:none";
                                        $complated = ($request['status'] == 2) ? "" : "display:none";
                                        $cancelled = ($request['status'] == 1) ? "" : "display:none";
                           
                                    ?>
                                        <tr>
                                            <td><?= $count++; ?></td>
                                            <td><?= getFullname($request['user_id']); ?></td>
                                            <td><?= $request['remarks']; ?></td>
                                            <td><?= date("F d, Y", strtotime($request['request_date'])); ?></td>
                                            <td>
                                                <?php
                                                if ($request['status'] == 0) {
                                                    echo "<span style='color: orange'>Pending</span>";
                                                } else if ($request['status'] == 1) {
                                                    echo "<span style='color: green'>Submitted</span>";
                                                } else if ($request['status'] == 2) {
                                                    echo "<span style='color: blue'>Approved</span>";
                                                } else if ($request['status'] == 3) {
                                                    echo "<span style='color: blue'>Completed</span>";
                                                } else {
                                                    echo "<span style='color: red'>Cancelled</span>";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <i data-toggle="tooltip" data-placement="bottom" title="View Request Details" onclick='window.location="<?= route("/request/view", $request["id"]) ?>"' style='cursor:pointer;background-color: #0cb4c9;padding: 5px;border-radius: 17px;' class='material-icons text-white btn-round'>remove_red_eye</i>

                                                <i data-toggle="tooltip" data-placement="bottom" title="Approve Request" onclick='window.location="<?= route("/request/status-to/approve", $request["id"]) ?>"' style='background-color: #0cb4c9;padding: 5px;border-radius: 17px;cursor:pointer;padding:5px;<?= $approve ?>' class="material-icons text-white">thumb_up</i>

                                                <i data-toggle="tooltip" data-placement="bottom" title="Complete Request" onclick='window.location="<?= route("/request/status-to/finish", $request["id"]) ?>"' style='background-color: #0cb4c9;padding: 5px;border-radius: 17px;cursor:pointer;padding:5px;<?= $complated ?>' class='material-icons text-white'>done_all</i>

                                                <i data-toggle="tooltip" data-placement="bottom" title="Cancel Request" onclick='window.location="<?= route("/request/status-to/cancel", $request["id"]) ?>"' style='background-color: #0cb4c9;padding: 5px;border-radius: 17px;cursor:pointer;padding:5px;<?= $cancelled ?>' class='material-icons text-white'>do_not_disturb</i>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#residents_request").dataTable();
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>