<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>


<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Requests</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary pull-right' onclick="window.location='<?= route('/request/send') ?>'" id='addpurok_btn'> Send Request </button>
                    </div>
                    <div class='col-sm-12'>
                        <div class="table-responsive">
                            <table class="table table-hover" id='residents_request'>
                                <thead class=" text-primary">
                                    <!-- <th></th> -->
                                    <th>#</th>
                                    <th>Request Date</th>
                                    <th>Date Added</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    foreach ($requests as $request) {
                                    ?>
                                        <tr>
                                            <td><?= $count++; ?></td>
                                            <td><?= date("F d, Y", strtotime($request['request_date'])) ?></td>
                                            <td><?= date("F d, Y", strtotime($request['date_added'])) ?></td>
                                            <td>
                                                <?php
                                                if ($request['status'] == 0) {
                                                    echo "<span style='color: orange'>Pending</span>";
                                                } else if ($request['status'] == 1) {
                                                    echo "<span style='color: green'>Submitted</span>";
                                                } else if ($request['status'] == 2) {
                                                    echo "<span style='color: blue'>Approved</span>";
                                                } else if ($request['status'] == 3) {
                                                    echo "<span style='color: blue'>Completed</span>";
                                                } else {
                                                    echo "<span style='color: red'>Cancelled</span>";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <i data-toggle="tooltip" data-placement="bottom" title="View Request Details" onclick="window.location='<?=route('/resident/view-details', $request['id'])?>'" style='cursor:pointer;background-color: #0cb4c9;padding: 5px;border-radius: 17px;' class='material-icons text-white btn-round'>remove_red_eye</i>

                                                <!-- <i class='material-icons text-success' onclick="window.location='<?=route('/resident/view-details', $request['id'])?>'">remove_red_eye</i> -->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#residents_request").dataTable();
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>