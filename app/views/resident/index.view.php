<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<div class="row">

    <div class="col-lg-12 col-md-6 col-sm-6">
        <?= alert_msg() ?>
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Residents Master List</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-md-6'>
                        <div class="md-form">
                            <select class='form-control' id='purokList' onchange='changeLocation()'>
                                <option value=''> &mdash; Please Choose Location &mdash;</option>
                                <?php
                                foreach ($puroks as $purok) {
                                ?>
                                    <option value='<?= $purok["id"] ?>'><?= $purok['name'] ?></option>
                                <?php } ?>
                            </select>
                            <label for="defaultForm-pass1">Purok Name</label>
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <!-- <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target='#addResident' id='addResident_btn'> Add Data </button> -->
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="table-responsive">
                            <table class="table table-hover" id='master_list'>
                                <thead class=" text-primary">
                                    <th>#</th>
                                    <th>NAME</th>
                                    <th>E-MAIL</th>
                                    <th>CONTACT</th>
                                    <th>CATEGORY</th>
                                    <th>STATUS</th>
                                    <th>ACTION</th>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require __DIR__ . '/add-resident-modal.php'; ?>

<script type="text/javascript">
    $(document).ready(function() {


    });
    function approveRegister(id){
        $("#updateBtn"+id).prop("disabled", true);
        $("#updateBtn"+id).html("Loading");
        $.post(base_url+"/resident/approve", {
            id: id
        }, function(res){
       
            alert_response_without_reload("All Good!", "User has been Successfully approved", "success");
            changeLocation();
        });
    }                                
    function changeLocation() {
        var purokList = $("#purokList").val();
        $("#master_list").DataTable().destroy();
        $('#master_list').dataTable({
            "processing": true,
            "ajax": {
                "url": base_url + "/resident/per-purok",
                "dataSrc": "data",
                "data": {
                    purokList: purokList
                },
                "type": "POST"
            },
            "columns": [{
                    "data": "count"
                },
                {
                    "data": "fullname"
                },
                {
                    "data": "email"
                },
                {
                    "data": "contact"
                },
                {
                    "data": "category"
                },
                {
                    "data": "status"
                },
                {
                    "data": "action"
                }

            ]
        });
    }
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>