<form method="POST" action="<?= route('/resident/save') ?>" id="add_resident">
    <div class="modal fade" id="addResident" tabindex="-1" role="dialog" aria-labelledby="addResidentLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addResidentLabel"><span class='fa fa-check-circle'></span> Add Resident to Master List.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= alert_msg() ?>
                    <h6 id="register-response" style="margin-bottom: 30px;"></h6>
                    <div class="md-form">
                        <input type="text" autocomplete='off' id="new_firstname" name="new_firstname" class="form-control" required>
                        <label for="new_firstname">Firstname <span style="color:red">*</span></label>
                    </div>
                    <div class="md-form" style='margin-top: 10px;'>
                        <input type="text" autocomplete='off' id="new_lastname" name="new_lastname" class="form-control" required>
                        <label for="new_lastname">Lastname <span style="color:red">*</span></label>
                    </div>
                    <div class="md-form" style='margin-top: 10px;'>
                        <select class='form-control' id='purokList' name='purokList'>
                            <option value=''> &mdash; Please Choose &mdash;</option>
                            <?php
                            foreach ($puroks as $purok) {
                            ?>
                                <option value='<?= $purok["id"] ?>'><?= $purok['name'] ?></option>
                            <?php } ?>
                        </select>
                        <label for="purokList">Purok Name <span style="color:red">*</span></label>
                    </div>
                    <div class="md-form" style='margin-top: 10px;'>
                        <select class='form-control' id='familyCat' name='familyCat'>
                            <option value=''> &mdash; Please Choose &mdash;</option>
                            <option value='head'> Head </option>
                            <option value='Member'> Member </option>
                        </select>
                        <label for="familyCat">Category <span style="color:red">*</span></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Add Resident</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>