<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Monthly Supply Report</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-6'>
                        <div class="md-form" style='margin-top: 10px;'>
                            <select onchange='supplyGraph(this)' class='form-control' id='supplies' name='supplies'>
                                <option value=''> &mdash; Please Choose &mdash;</option>
                                <?php
                                foreach ($supply as $supplys) {
                                ?>
                                    <option value='<?=$supplys["id"] ?>'><?= $supplys['supply_name'] ?></option>
                                <?php } ?>
                            </select>
                            <label for="purokList">Supply <span style="color:red">*</span></label>
                        </div>
                    </div>
                    <div class='col-sm-12'>
                        <div id='chart' style='width:100% !important; margin-top: 20px;'>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function(){
      
    });
function supplyGraph(supply){
    var supply_id = supply.value;
    $("#chart").html("<span class='fa fa-spin fa-spinner'></span>").css("text-align", "center").css("font-size", "50px");

    $.ajax({
        type: "POST",
        url: base_url + "/report/supply-report",
        data: {
            supply_id: supply_id
        },
        dataType: "json",
        success: function (data) {
            console.log(data)
            Highcharts.chart("chart", {
            chart: {
                type: 'column'
            },
            title: {
                useHTML: true,
                text: "Supply Request Per Month"
            },
            xAxis: {
                type: 'category',
                title: {
                text: "Date"
                }
            },
            yAxis: {
                title: {
                text: 'Quantity'
                }
            },
            plotOptions: {
                column: {
                    dataLabels: {
                    enabled: true
                    },
                    enableMouseTracking: true
                },
                series: {
                    connectNulls: true
                }
            },

            tooltip: {
                headerFormat: '',
                pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title}</span>: <b>{point.y:.0f}</b><br/>'
            },

            series: data['series']
            });
        },
        error: function (data) {
                alert(data);
            }
    })
}
</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>