<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Inventory Report</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class='col-sm-12'>
                <label for="">Inventory As Of <?=date("F d, Y")?></label>
                    <div class="table-responsive">
                        <table class="table table-hover" id='sms_list'>
                            <thead class=" text-primary">
                                <th>#</th>
                                <th>UNIT OF MEASURE</th>
                                <th>ITEM</th>
                                <th>IN</th>
                                <th>OUT</th>
                                <th>EXPIRED</th>
                                <th>REMAINING</th>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($supply as $supplies) {
                                    $unitofmeasure = $supplies['stock_measure']."".$supplies['unit_measure'];
                                    $inventory_IN = INVin($supplies['supply_id'], $unitofmeasure);
                                    $inventory_OUT = INVout($supplies['supply_id'], $unitofmeasure);
                                    $inventory_EXP = getTotalExpired($supplies['supply_id'], $unitofmeasure);
                                    $remaining = $inventory_IN - ($inventory_OUT + $inventory_EXP);

                                    $style = ($remaining < 10)?"background-color: #ffa8a88f":"";
                                ?>
                                    <tr style='<?=$style?>'>
                                        <td><?= $count++; ?></td>
                                        <td><?= (empty($supplies['unit_measure']))?"<span style='color: red'>N/A</span>":$supplies['stock_measure']."".$supplies['unit_measure'] ?></td>
                                        <td><?= $supplies['supply_name'] ?></td>
                                        <td><?= $inventory_IN; ?></td>
                                        <td><?= $inventory_OUT; ?></td>
                                        <td><?= $inventory_EXP; ?></td>
                                        <td><?= $remaining; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
 $(document).ready(function() {
    $("#sms_list").DataTable({
        "paging":   false,
        "ordering": false,
        "info":     false
    });

    
});

</script>
<?php require __DIR__ . '/../layouts/footer.php'; ?>