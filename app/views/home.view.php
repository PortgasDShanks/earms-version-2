<?php

use App\Core\Auth;
use App\Core\Request;

require 'layouts/head.php'; ?>

<style>
    .welcome-msg {
        margin-top: 10%;
    }
</style>
<div class="row">
    <div class="col-12">
        <div class="d-flex flex-column align-items-center justify-content-center">
            <h2 class="mb-0 text-muted welcome-msg">Welcome to Brgy. Tangub Emergency Assistance Application</h2>
            <p class="text-muted">Your request is our priotity</p>
        </div>
    </div>
</div>

<?php require 'layouts/footer.php'; ?>

