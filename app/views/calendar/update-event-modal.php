<?php 
use App\Core\Auth;
?>
<form method="POST" id='updateEvents'>
    <div class="modal fade" id="showEvents" tabindex="-1" role="dialog" aria-labelledby="showEventsLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    
                    <?php if(getRole(Auth::user('role_id')) == 'SA') { ?>
                        <h5 class="modal-title" id="showEventsLabel">
                        &mdash; Update Event &mdash;</h5>
                    <?php } else { ?>
                        <h5 class="modal-title" id="showEventsLabel">
                        &mdash; View Event &mdash;</h5>
                    <?php } ?>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <input type="hidden" id="eventID" name="eventID">
                        <div class='col-sm-12'>
                            <div class="md-form">
                                <input type="text" autocomplete='off' id="show_name" name="show_name" class="form-control" required>
                                <label for="show_name">Event Name <span style="color:red">*</span></label>
                            </div>
                        </div>
                        <br>
                        <div class='col-sm-12' style='margin-top:20px'>
                            <div class="md-form">
                                <input type="text" autocomplete='off' id="show_desc" name="show_desc" class="form-control" required>
                                <label for="show_desc">Event Description <span style="color:red">*</span></label>
                            </div>
                        </div>
                        <!-- <br>
                        <div class='col-sm-6' style='margin-top:20px'>
                            <div class="md-form">
                                <input type="date" autocomplete='off' id="show_date_from" name="show_date_from" class="form-control" required>
                                <label for="show_date_from">Date From <span style="color:red">*</span></label>
                            </div>
                        </div>

                        <div class='col-sm-6' style='margin-top:20px'>
                            <div class="md-form">
                                <input type="date" autocomplete='off' id="show_date_to" name="show_date_to" class="form-control" required>
                                <label for="show_date_to">Date To <span style="color:red">*</span></label>
                            </div>
                        </div> -->

                    </div>
                </div>
                <div class="modal-footer">
                <?php if(getRole(Auth::user('role_id')) == 'SA') { ?>
                    <button type="submit" id="update_btn" class="btn btn-primary btn-sm">Save changes</button>
                <?php } ?>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>