<?php

use App\Core\Request;
use App\Core\Auth;
require __DIR__ . '/../layouts/head.php';
?>

<?= alert_msg() ?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Calendar of Events</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <?php if(getRole(Auth::user('role_id')) == 'SA') { ?>
                    <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary' onclick='showModal()' style='float: right'> <span class='feather icon-plus-circle'></span> Add Event</button>
                    </div>
                    <?php } ?>
                    <div class='col-sm-12 col-xs-12' style='margin-top: 20px;'>
                        <div id='calendar'>
                        
                        </div>
                    </div>
                    <?php if(getRole(Auth::user('role_id')) == 'SA') { ?>
                    <div class='col-sm-12' id='calendarTrash'>
                        <button style='font-size: 2em;' class='btn btn-sm btn-danger btn-block'> <span class='material-icons' style='font-size: 2em'>delete_forever</span> Event Trash</button>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__ . '/add-event-modal.php'; ?>
<?php include __DIR__ . '/update-event-modal.php'; ?>
<script>
function showModal(){
    $("#addEvents").modal();
}

function dropEvent(new_s, new_e, id){
    $.post(base_url+"/calendar/drop-event", {
        new_s: new_s,
        new_e: new_e,
        id: id
    }, function(res){
        if(res > 0){
            alert_response("All Good!", "Event Date has been Successfully changed", "success");
        }else{
            alert_response_without_reload("Opps!", "Something went wrong while saving data", "danger");
        }
    })
}
function deleteEvent(id){
    $.post(base_url+"/calendar/delete-event", {
        id: id
    }, function(res){
        if(res > 0){
            alert_response("All Good!", "Event has been Successfully deleted", "success");
        }else{
            alert_response_without_reload("Opps!", "Something went wrong while saving data", "danger");
        }
    });
}
$(document).ready(function() {

    $("#saveEvents").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/calendar/add-event";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            alert_response("All Good!", "Event has been Successfully added", "success");
        });
    });

    $("#updateEvents").on('submit', function(e){
        e.preventDefault();
        $("#update_btn").prop("disabled", true);
        $("#update_btn").html("Loading");
        const url = base_url+"/calendar/update-event";
        const data = $(this).serialize();
        $.post(url, data, function(res){
            $("#showEvent").modal('hide');
            if(res > 0){
                alert_response("All Good!", "Event has been Successfully updated", "success");
            }else{
                alert_response("Opps!", "Something went wrong while saving data", "danger");
            }
        });
    });

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'year,month,agendaWeek,agendaDay'
        },

        defaultDate: '<?= date('Y-m-d'); ?>',
        editable: true,
        eventLimit: true,
        droppable: true,
        resizable: true,
        dayRender: function (date, cell) {
            
            var newdate = new Date(date);
            yr      = newdate.getFullYear(),
            month   = newdate.getMonth() < 10 ? '0' + newdate.getMonth() : newdate.getMonth(),
            day     = newdate.getDate()  < 10 ? '0' + newdate.getDate()  : newdate.getDate(),
            newDate = yr + '-' + month + '-' + day;
        
        },
        events:[
            <?php 
                $ctrEvent= 1;
                $user = Auth::user('id');
                $event = DB()->selectLoop("*","events")->get();
                $count = count($event);
                foreach ($event as $events) {
                    $event_title = $events['event_name'];
                    $event_desc = $events['event_description'];
                    $start_date = $events['event_date_from'];
                    $end_date = date("Y-m-d", strtotime('+1 day', strtotime($events['event_date_to'])));
                
            ?>
            {
              title: '<?=$event_title." "."--"." ".$event_desc; ?>',
              start: '<?=$start_date;?>',
              end: '<?=$end_date;?>',
              color: '#3498db',
              id: <?=$events['id']?>,
              description: '<?=$events["event_description"]?>',
              event_date: '<?=date("F d, Y", strtotime($events['event_date_from']))." - ".date("F d,Y", strtotime($events['event_date_to'])); ?>',
              titles: '<?=$event_title;?>'
          
            }<?php 
              if($ctrEvent < $count){ 
                echo ",";
              }
              $ctrEvent++;
            }
            ?>
          ],
          eventDrop: function(event , delta , revertfunc){
                var title = event.title;
                var change_date = event.start.format();
                var end_change_date = event.end.format();
                var id = event.id;
                var conf = confirm("The Date of this event will update. Do you want to continue?");
                if(conf == true){
                    dropEvent(change_date,end_change_date,id);
                }else{
                    alert_response("Update Aborted!","Changes was not saved","warning");
                }
            },
            eventDragStop: function(event,jsEvent) {
              const id = event.id
              const trashEl = jQuery('#calendarTrash');
              const ofs = trashEl.offset();

              const x1 = ofs.left;
              const x2 = ofs.left + trashEl.outerWidth(true);
              const y1 = ofs.top;
              const y2 = ofs.top + trashEl.outerHeight(true);

              if (jsEvent.pageX >= x1 && jsEvent.pageX <= x2 &&
                  jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
                   var conf = confirm("This event will be deleted. Would you like to continue?");
                   if(conf == true){
                        deleteEvent(id);
                   }else{
                        alert_response("Delete Aborted!","Changes was not saved","warning");
                   }
              }
            
          },
          eventResize: function(info) {
                alert(info.event.title + " end is now " + info.event.end.toISOString());

                if (!confirm("is this okay?")) {
                info.revert();
                }
            },
          eventClick: function(callEvent , jsEvent, view){
    
                $("#showEvents").modal();

                $("#show_name").val(callEvent.titles);
                $("#show_desc").val(callEvent.description);
                // $("#show_date_from").val(callEvent.start);
                // $("#show_date_to").val(callEvent.end);
                $("#eventID").val(callEvent.id);
            
            },
            eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2100-01-01'
            }
    });
});

 
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>