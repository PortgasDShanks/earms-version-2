<form method="POST" id='saveEvents'>
    <div class="modal fade" id="addEvents" tabindex="-1" role="dialog" aria-labelledby="addEventsLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addEventsLabel">&mdash; Add Event &mdash;</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class='col-sm-12'>
                            <div class="md-form">
                                <input type="text" autocomplete='off' id="event_name" name="event_name" class="form-control" required>
                                <label for="event_name">Event Name <span style="color:red">*</span></label>
                            </div>
                        </div>
                        <br>
                        <div class='col-sm-12' style='margin-top:20px'>
                            <div class="md-form">
                                <input type="text" autocomplete='off' id="event_desc" name="event_desc" class="form-control" required>
                                <label for="event_desc">Event Description <span style="color:red">*</span></label>
                            </div>
                        </div>
                        <br>
                        <div class='col-sm-6' style='margin-top:20px'>
                            <div class="md-form">
                                <input type="date" autocomplete='off' id="event_date_from" name="event_date_from" class="form-control" required>
                                <label for="event_date_from">Date From <span style="color:red">*</span></label>
                            </div>
                        </div>

                        <div class='col-sm-6' style='margin-top:20px'>
                            <div class="md-form">
                                <input type="date" autocomplete='off' id="event_date_to" name="event_date_to" class="form-control" required>
                                <label for="event_date_to">Date To <span style="color:red">*</span></label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary btn-sm">Save changes</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>