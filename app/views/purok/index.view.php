<?php

use App\Core\Request;

require __DIR__ . '/../layouts/head.php';
?>

<?= alert_msg() ?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Purok Master List</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target='#addPurok' id='addpurok_btn'> Add Data </button>
                    </div>
                    <div class='col-sm-12'>
                        <div class="table-responsive">
                            <table class="table table-hover table-striped" id='master_list'>
                                <thead class=" text-primary">
                                   <tr>
                                        <th>#</th>
                                        <th>PUROK NAME</th>
                                        <th>ACTION</th>
                                   </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    foreach ($puroks as $purok) {
                                    ?>
                                        <tr>
                                            <td><?= $count++; ?></td>
                                            <td><input type="text" class='form-control' id='suppname<?=$purok['id']?>' value='<?= $purok['name']; ?>'></td>
                                            <td>
                                                <i data-toggle="tooltip" data-placement="bottom" title="Delete Purok in List" style='cursor:pointer;background-color: #0cb4c9;padding: 5px;border-radius: 17px;' class='material-icons text-white btn-round' onclick='deletePurok(<?=$purok["id"]?>)' id='deleteBtn<?=$purok['id']?>'>delete</i>

                                                <i data-toggle="tooltip" data-placement="bottom" title="Update Details" style='cursor:pointer;background-color: #0cb4c9;padding: 5px;border-radius: 17px;' class='material-icons text-white btn-round' onclick="updatePurok(<?=$purok['id']?>)" id='updateBtn<?=$purok['id']?>'>edit</i>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__ . '/add-purok-modal.php'; ?>

<script>
    $(document).ready(function() {
        $("#master_list").DataTable();
    });

    $("#add_purok").on("submit", function(e) {
        e.preventDefault();
        $("#create_btn").prop("disabled", true);
        $("#create_btn").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        var url = base_url + '/purok/save';
        var data = $(this).serialize();
        $.post(url, data, function(data) {
            if (data == 1) {
                alert_response("All Good!", "Purok Successfully Added", "success");
            } else {
                alert_response("Oppss!", "There was an error encountered", "error");
            }
        });
    });

    function deletePurok(id){
        $("#deleteBtn"+id).prop("disabled", true);
        $("#deleteBtn"+id).html("Loading");
        $.post(base_url+"/purok/delete", {
            id: id
        }, function(res){
            if (res == 1) {
                alert_response("All Good!", "Purok Successfully Deleted", "success");
            } else {
                alert_response("Oppss!", "There was an error encountered", "error");
            }
        });
    }

    function updatePurok(id){
        const suppname = $("#suppname"+id).val();
        $("#updateBtn"+id).prop("disabled", true);
        $("#updateBtn"+id).html("Loading");
        $.post(base_url+"/purok/update", {
            id: id,
            suppname: suppname
        }, function(res){
            if (res == 1) {
                alert_response("All Good!", "Purok Successfully Updated", "success");
            } else {
                alert_response("Oppss!", "There was an error encountered", "error");
            }
        });
    }
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>