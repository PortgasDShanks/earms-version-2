<form method="POST" id="add_purok">
    <div class="modal fade" id="addPurok" tabindex="-1" role="dialog" aria-labelledby="addPurokLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addPurokLabel"><span class='fa fa-check-circle'></span> Add Purok to Master List.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="card form-white2" style="background-color: #cecece91!important">
                            <div class="card-body">
                                <h6 id="register-response" style="margin-bottom: 30px;"></h6>
                                <div class="md-form">
                                    <input type="text" autocomplete='off' id="purok_name" name="purok_name" class="form-control" required>
                                    <label for="purok_name">Purok Name <span style="color:red">*</span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>