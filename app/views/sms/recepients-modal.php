<div class="modal fade" id="smsrecepientsModal" tabindex="-1" role="dialog" aria-labelledby="smsrecepientsLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="smsrecepientsLabel"><span class='fa fa-check-circle'></span> SMS Recepients.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hover" id='recepients' style='width:100%'>
                            <thead class="text-primary">
                                <th>#</th>
                                <th>NAME</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="send_sms_btn" class="btn btn-primary">Send</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>