<form method="POST" id="sms_send">
    <div class="modal fade" id="smsModal" tabindex="-1" role="dialog" aria-labelledby="smsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="smsModalLabel"><span class='fa fa-check-circle'></span> Send SMS.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="card form-white2" style="background-color: #cecece91!important">
                            <div class="card-body">
                                <h6 id="register-response" style="margin-bottom: 30px;"></h6>
                                <div class="md-form">
                                    <textarea rows='4' style='resize:none' class='form-control' name='sms_content' id='sms_content' placeholder='Type your message here...'></textarea>
                                    <label for="sms_content">Message <span style="color:red">*</span></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="send_sms_btn" class="btn btn-primary">Send</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>