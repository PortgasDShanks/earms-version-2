<?php

use App\Core\Request;
use App\Core\App;
require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">SMS</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-sm-12'>
                        <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target='#smsModal' id='supplyCategoryBtn'> Send SMS </button>
                    </div>
                    <div class='col-sm-12'>
                        <div class="table-responsive">
                            <table class="table table-hover" id='sms_list'>
                                <thead class=" text-primary">
                                    <th>#</th>
                                    <th>Message</th>
                                    <th>Date and Time</th>
                                    <th>Recipient/s</th>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    foreach ($msgs as $msg) {
                                    ?>
                                        <tr>
                                            <td><?= $count++; ?></td>
                                            <td><?= $msg['sms_content'] ?></td>
                                            <td><?= date("F d, Y -- h:i A", strtotime($msg['sms_datetime'])) ?></td>
                                            <td>
                                                <a onclick='showRecipients(<?= $msg["id"] ?>)'>
                                                    <button class="btn btn-primary btn-sm">Show More...</button>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__ . '/send-sms-modal.php'; ?>
<?php include __DIR__ . '/recepients-modal.php'; ?>

<script>
    function showRecipients(id) {
        recepients_data(id);
        $("#smsrecepientsModal").modal();
    }

    function recepients_data(id) {
        $("#recepients").DataTable().destroy();
        $('#recepients').dataTable({
            "processing": true,
            "ajax": {
                "url": base_url + "/sms/recepients",
                "dataSrc": "data",
                "data": {
                    id: id
                },
                "type": "POST"
            },
            "columns": [{
                    "data": "count"
                },
                {
                    "data": "fullname"
                }

            ]
        });
    }

    $(document).ready(function() {
        $("#sms_list").DataTable();

        $("#sms_send").on('submit', function(e){
            e.preventDefault();
            const url = base_url+"/sms/send";
            const data = $(this).serialize();
            $.post(url, data, function(res){
                alert_response_without_reload("All Good!", "SMS Successfully Sent!", "success");
            });
        });
    });
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>