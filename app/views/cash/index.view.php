<?php

use App\Core\Request;
use App\Core\Auth;
require __DIR__ . '/../layouts/head.php';
?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Cash Assistance</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-md-12'>
                        <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target='#addRequest' id='addResident_btn'> Request Cash Assistance </button>
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="table-responsive">
                            <table class="table table-hover" id='master_list'>
                                <thead class=" text-primary" style='text-align: center'>
                                    <th>#</th>
                                    <th>REASON</th>
                                    <th>AMOUNT</th>
                                    <th>REQUEST DATE</th>
                                    <th>STATUS</th>
                                </thead>
                                <tbody style='text-align: center'>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__ . '/request-modal.php'; ?>
<?php //include __DIR__ . '/update-event-modal.php'; ?>
<script>

$(document).ready(function() {
    cashAssistanceData();

    $("#requestCash").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/cash/request-new";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            if(res > 0){
                alert_response("All Good!", "Request for Cash Assistance Successfully sent. Please wait for an E-mail for the status of your request.", "success");
            }else{
                alert_response("Opps!", "Something went wrong while saving data", "danger");
            }
        });
    });
});
function cashAssistanceData() {
    $("#master_list").DataTable().destroy();
    $('#master_list').dataTable({
        "processing": true,
        "ajax": {
            "url": base_url + "/cash/cash-assistance",
            "dataSrc": "data",
            "data": {},
            "type": "POST"
        },
        "columns": [{
                "data": "count"
            },
            {
                "data": "reason"
            },
            {
                "data": "amount"
            },
            {
                "data": "date_req"
            },
            {
                "data": "status"
            }

        ]
    });
}
 
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>