<form method="POST" id='requestCash'>
    <div class="modal fade" id="addRequest" tabindex="-1" role="dialog" aria-labelledby="addRequestLabel" aria-hidden="true" data-backdrop='static'>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addRequestLabel">&mdash; New Request for Cash Assistance &mdash;</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class='col-sm-12'>
                            <div class="md-form">
                                <input type="date" autocomplete='off' id="request_date" name="request_date" class="form-control" required>
                                <label for="request_date">Request Date <span style="color:red">*</span></label>
                            </div>
                        </div>
                        <div class='col-sm-12' style='margin-top:20px'>
                            <div class="md-form">
                                <input type="number" autocomplete='off' id="cash_amount" name="cash_amount" class="form-control" required>
                                <label for="cash_amount">Amount <span style="color:red">*</span></label>
                            </div>
                        </div>
                        <br>
                        <div class='col-sm-12' style='margin-top:20px'>
                            <div class="md-form">
                                <input type="text" autocomplete='off' id="request_reason" name="request_reason" class="form-control" required>
                                <label for="request_reason">Reason <span style="color:red">*</span></label>
                            </div>
                        </div>
                        <br>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="create_btn" class="btn btn-primary btn-sm">Save changes</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>