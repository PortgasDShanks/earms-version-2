<?php

use App\Core\Request;
use App\Core\Auth;
require __DIR__ . '/../layouts/head.php';
?>

<?= alert_msg() ?>

<div class="row">
    <div class="col-lg-12 col-md-6 col-sm-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h4 class="card-title">Cash Assistance</h4>
                <p class="card-category">Manage your community accurately</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-md-12' style='margin-top:10px;'>
                        <div class="table-responsive">
                            <table class="table table-hover" id='master_list'>
                                <thead class=" text-primary" style='text-align: center'>
                                    <th>#</th>
                                    <th>NAME</th>
                                    <th>AMOUNT</th>
                                    <th>REASON</th>
                                    <th>REQUEST DATE</th>
                                    <th>STATUS</th>
                                    <th>ACTION</th>
                                </thead>
                                <tbody style='text-align: center'>
                                    <?php 
                                        $count = 1; 
                                        foreach ($cash_list as $lists) { 
                                        $approve = ($lists['status'] == 0)?"":"display:none";
                                    ?>
                                        <tr>
                                            <td><?=$count++;?></td>
                                            <td><?=$lists['fullname']?></td>
                                            <td><?=number_format($lists['cash_amount'], 2)?></td>
                                            <td><?=$lists['reason']?></td>
                                            <td><?=date("F d, Y", strtotime($lists['date_request']))?></td>
                                            <td><?=($lists['status'] == 0)?"<span style='color:orange'>Pending</span>":(($lists['status'] == 1)?"<span style='color:green'>Approved</span>":"<span style='color:red'>Cancelled</span>")?></td>
                                            <td>

                                                <i data-toggle="tooltip" data-placement="bottom" title="Cancel Request" onclick="cancelRequest(<?=$lists['cid']?>)" id='cancelR<?=$lists['cid']?>' style='cursor:pointer;background-color: #0cb4c9;padding: 5px;border-radius: 17px;<?=$approve?>' class='material-icons text-white btn-round'>block</i>

                                                <i data-toggle="tooltip" data-placement="bottom" title="Approve Request" onclick="approveRequest(<?=$lists['cid']?>)" id='approveR<?=$lists['cid']?>' style='background-color: #0cb4c9;padding: 5px;border-radius: 17px;cursor:pointer;padding:5px;<?=$approve?>' class="material-icons text-white">thumb_up</i>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include __DIR__ . '/request-modal.php'; ?>
<?php //include __DIR__ . '/update-event-modal.php'; ?>
<script>
function approveRequest(id){
    $("#approveR"+id).prop('disabled', true);
    $.post(base_url+"/cash/approve-request" ,{
        id: id
    }, function(res){
        alert_response("All Good!", "Request for Cash Assistance Successfully approved!", "success");

        $("#approveR").prop('disabled', false);
    });
}

function cancelRequest(id){
    $("#cancelR"+id).prop('disabled', true);
    $.post(base_url+"/cash/cancel-request" ,{
        id: id
    }, function(res){
        alert_response("All Good!", "Request for Cash Assistance Successfully cancelled!", "success");

        $("#cancelR").prop('disabled', false);
    });
}
$(document).ready(function() {
    $("#master_list").DataTable();


    $("#requestCash").on('submit', function(e){
        e.preventDefault();
        var url = base_url+"/cash/request-new";
        var data = $(this).serialize();
        $.post(url, data, function(res){
            if(res > 0){
                alert_response("All Good!", "Request for Cash Assistance Successfully sent. Please wait for an E-mail for the status of your request.", "success");
            }else{
                alert_response("Opps!", "Something went wrong while saving data", "danger");
            }
        });
    });
});

 
</script>

<?php require __DIR__ . '/../layouts/footer.php'; ?>