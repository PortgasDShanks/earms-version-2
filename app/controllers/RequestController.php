<?php

namespace App\Controllers;

use App\Core\Auth;
use App\Core\Request;

use SMSGatewayMe\Client\ApiClient;
use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\Api\MessageApi;
use SMSGatewayMe\Client\Model\SendMessageRequest;

class RequestController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Request";

        $requests = DB()->selectLoop("*", "request ORDER BY date_added DESC")->get();

        // $this->readAllNotif($requests);

        return view('/request/index', compact('pageTitle', 'requests'));
    }

    public function readAllNotif($requests)
    {
        $ids = [];
        $type = getRole(Auth::user('role_id'));
        foreach ($requests as $request) {
            $ids[] = $request['id'];
        }

        $collectedIds = implode("','", $ids);
        DB()->delete('notif', "request_id in('$collectedIds') AND notif_type = '$type'");
    }

    public function send()
    {
        $pageTitle = "Send Request";

        $supplyTypes = DB()->selectLoop("*", "supply_type")->get();

        return view('/request/send', compact('pageTitle', 'supplyTypes'));
    }

    public function saveHead()
    {
        $request = Request::validate('/request/send', [
            "requestDate" => ['required'],
            "remarks" => ['required']
        ]);

        $form_to_request = [
            "user_id" => Auth::user('id'),
            "request_date" => $request['requestDate'],
            "remarks" => $request['remarks'],
            "date_added" => date('Y-m-d H:i:s'),
            "status" => 0
        ];

        $response = DB()->insert("request", $form_to_request, "Y");

        echo $response;
    }

    public function itemsPerOption()
    {
        $request = Request::validate('', [
            "option" => ['required']
        ]);

        $supplies = DB()->selectLoop("*", "supplies", "supply_type = '$request[option]' AND supply_status = 0")->get();

      
        if (count($supplies) > 0) {
      
            $data = "<option value='' disabled selected>&mdash; Please Choose an Option &mdash; </option>";
            
            foreach ($supplies as $supply) {
                $data .= "<option value='" . $supply['id'] . "'>" . $supply['supply_name'] . "</option>";
            }

      
        } else {
            $data = 0;
        }

        echo $data;
    }

    public function showMeasures()
    {
        $request = Request::validate('', [
            "option_val" => ['required']
        ]);

        $supplies = DB()->selectLoop("*", "supply_stocks", "supply_id = '$request[option_val]' AND unit_measure != '' GROUP BY CONCAT(stock_measure,'',unit_measure)")->get();
        $data = "<option value=''>&mdash; Please Choose an Option &mdash; </option>";
        foreach ($supplies as $supply) {
            $data .= "<option value='" . $supply['stock_measure'] . "" . $supply['unit_measure'] . "'>" .$supply['stock_measure'] . "" . $supply['unit_measure'] . "</option>";
        }

        echo $data;
    }

    public function inventory()
    {
        $request = Request::validate();

        $stocks_total = DB()->select("sum(quantity) as ttl", "supply_stocks", "supply_id = '$request[option_val]' AND CONCAT(stock_measure,'',unit_measure) = '$request[unitMeasurement]'")->get();

        $remaining = $stocks_total['ttl'] - (getTotalExpired($request['option_val'], $request['unitMeasurement']) + INVout($request['option_val'], $request['unitMeasurement']));

        echo $remaining;
    }

    public function saveDetail()
    {
        $request = Request::validate('', [
            "option_val" => ['required'],
            "qntty" => ['required'],
            "optionSelected" => ['required'],
            "headerID" => ['required'],
            "unitMeasurement" => ['required']
        ]);

        $form_to_request_detail = [
            "header_id" => $request['headerID'],
            "supply_id" => $request['option_val'],
            "quantity" => $request['qntty'],
            "unit_of_measure" => $request['unitMeasurement'],
            "category" => $request['optionSelected']
            
        ];

        $response = DB()->insert("request_detail", $form_to_request_detail);
        echo $response;
    }

    public function detailList()
    {
        $request = Request::validate('', [
            "headerID" => ['required']
        ]);

        $details = DB()->selectLoop("r.id as hid, d.id as did, m.supply_name, d.quantity, st.type_name, r.status, d.unit_of_measure", "request as r, request_detail as d, supplies as m, supply_type as st", "r.id = d.header_id AND d.supply_id = m.id AND m.supply_type = st.id AND r.id = '$request[headerID]'")->get();

        $count = 1;
        $response['data'] = [];
        $list = [];
        foreach ($details as $detail) {
            $list = array();
            $list['id'] = $detail['did'];
            $list['count'] = $count++;
            $list['supply_name'] = $detail['supply_name'];
            $list['unitMeasure'] = $detail['unit_of_measure'];
            $list['quantity'] = number_format($detail['quantity'],0);
            $list['category'] = $detail['type_name'];
            $list['action'] = ($detail['status'] == 0) ? "<i onclick='delete_item_from_request(".$detail['did'].")' id='delete_btn" . $detail['did'] . "' style='cursor:pointer' class='material-icons text-danger'>delete_forever</i>" : "";

            array_push($response['data'], $list);
        }

        echo json_encode($response);
    }

    public function finish()
    {
        $request = Request::validate('', [
            "headerID" => ['required']
        ]);

        $form_to_notif = [
            "user_id" => Auth::user('id'),
            "request_id" => $request['headerID'],
            "date_added" => date('Y-m-d H:i:s'),
            "status" => 0,
            "notif_type" => 'SA',
            "notif_category" => "SR"
        ];

        $response = DB()->update("request", ["status" => 1], "id = '$request[headerID]'");
        DB()->insert("notif", $form_to_notif);

        echo $response;
    }

    public function delete()
    {
        $request = Request::validate();

        $response = DB()->delete("request_detail", "id = '$request[id]'");

        echo $response;
    }

    public function viewRequest($id)
    {
        $pageTitle = "View Request";

        $requests = DB()->select("u.fullname, r.remarks, r.request_date, r.status", "request as r, users as u", "r.user_id = u.id AND r.id = '$id'")->get();

        $approve = ($requests['status'] == 1) ? "" : "display:none";
        $complated = ($requests['status'] == 2) ? "" : "display:none";
        $cancelled = ($requests['status'] != 2 && $requests['status'] != 0) ? "" : "display:none";


        return view('/request/view-by-admin', compact('pageTitle', 'requests', 'id', 'approve', 'complated', 'cancelled'));
    }

    public function updateStatusTo($status, $id)
    {
        $statusInt = ($status == 'approve') ? 2 : (($status == 'finish') ? 3 : 4);

        $smsbody = ($status == 'approve')?"Your Request has been approved! Please go to brgy. gym to claim your request":"We're sorry but your request has been cancelled!";
        $device_id = 127070;
        DB()->update("request", ["status" => "$statusInt"], "id = '$id'");

        $request = DB()->select("status, user_id", "request", "id = '$id'")->get();
        if ($request['status'] == 2 || $request['status'] == 4) {
            $contact = DB()->select("contact","users","id = $request[user_id]")->get();

            $data = array(
                "phone_number" => $contact['contact'],
                "message" => $smsbody,
                "device_id" => $device_id
            );
            $sms = $this->sendSMS($data);

            $form_to_notif = [
                "user_id" => $request['user_id'],
                "request_id" => $id,
                "date_added" => date('Y-m-d H:i:s'),
                "status" => 0,
                "notif_type" => "SA"
            ];
            $user = DB()->select("u.fullname, u.email", "users as u, request as r", "r.id = '$id'")->get();
            $this->sendEmail($user['email'], $user['fullname'], $status);

            $response = DB()->insert("notif", $form_to_notif);
            $message = ($response == 1)
                ? ["message" => "All Good! Request has been {$status}.", "status" => "success"]
                : ["message" => "Oppss! There was an error encountered.", "status" => "danger"];
        } else {
            $message = ["message" => "Request has been set to Done.", "status" => "success"];
        }

        redirect('/request', $message);
    }

    public function sendEmail($email_address, $fullname, $action)
    {
        $subject = "Brgy. Tangub Emergency Assistance - Request Status";

        $body = "<!DOCTYPE html>
                    <html>
                        <head>
                            <title></title>
                            <link rel='stylesheet' href='".public_url('/assets/adminty/bower_components/bootstrap/css/bootstrap.min.css')."'>
                        </head>
                        <body><h1>Hi ".ucwords($fullname).",</h1>
                    <p>Your Request has been ".$action." by the brgy. Please go to Brgy. Gym and claim your request. Thank you</p>
                    
                    <p>Best Regards,<br />Management Team</p>
                    Please do not reply.
                        </body>
                    </html>";

        $recepients = $email_address;

        $sent = sendMail($subject, $body, $recepients);

        return $sent;
    }

    public function sendSMS($text)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://smsgateway.me/api/v4/message/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "[".json_encode($text)."]",
            CURLOPT_HTTPHEADER => array(
                "authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTYzNDIwNDAzMywiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjg4NTkyLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.pmvs1PV48-WOaF8YqxGGlCSjhtqefzu0ccmdyjPd3w4",
                "cache-control: no-cache"
            ),
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        echo $err;
    }
}
