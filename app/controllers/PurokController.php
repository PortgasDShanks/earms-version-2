<?php

namespace App\Controllers;

use App\Core\Auth;
use App\Core\Request;

class PurokController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Purok";
        $puroks = DB()->selectLoop("*", "purok_masterlist")->get();

        return view('/purok/index', compact('pageTitle', 'puroks'));
    }

    public function store()
    {
        $request = Request::validate('', [
            "purok_name" => ['required']
        ]);

        $dataToInsert = [
            "name" => $request['purok_name']
        ];

        $response = DB()->insert("purok_masterlist", $dataToInsert);

        echo $response;
    }

    public function delete()
    {
        $request = Request::validate();

        $response = DB()->delete('purok_masterlist', "id = '$request[id]'");

        echo $response;
    }

    public function update()
    {
        $request = Request::validate();

        $response =  DB()->update("purok_masterlist", ["name" => $request['suppname']], "id = '$request[id]'");

        echo $response;
    }
}
