<?php

namespace App\Controllers;

use App\Core\Auth;
use App\Core\Request;

class CashAssistanceController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Cash Assistance";
        
        $auth = Auth::user('id');

        $cash_list = DB()->selectLoop("*","cash_assistance","user_id = '$auth' ORDER BY id DESC")->get();

        return view('/cash/index', compact('pageTitle', 'cash_list'));
    }

    public function requests()
    {
        $pageTitle = "Cash Assistance Requests";
        
        $auth = Auth::user('id');

        $cash_list = DB()->selectLoop("c.id as cid, u.fullname, c.cash_amount, c.reason, c.date_request, c.status", "cash_assistance as c, users as u", "c.user_id = u.id ORDER BY c.id DESC ")->get();

        return view('/cash/assistance-request', compact('pageTitle', 'cash_list'));
    }

    public function approve()
    {
        $request = Request::validate();
        $auth = Auth::user('id');
        $data = [
            "status" => 1
        ];

        $response = DB()->update("cash_assistance", $data, "id = '$request[id]'");

        $user = DB()->select("*", "cash_assistance as c, users as u", "c.user_id = u.id AND c.id = '$request[id]'")->get();

        $content = "Your Requested Cash Assistance has been approved by the Brgy. Please go to the Brgy. Gym for more details. Thank you";

        $notif = [
            "user_id" => $auth,
            "request_id" => $request['id'],
            "notif_type" => "U",
            "notif_category" => "CA"
        ];

        DB()->insert("notif", $notif);

        $email = $this->sendEmail($user['email'], $user['fullname'], $content);

        echo $user['email'];
    }

    public function cancel()
    {
        $request = Request::validate();
        $auth = Auth::user('id');

        $data = [
            "status" => 2
        ];

        $response = DB()->update("cash_assistance", $data, "id = '$request[id]'");

        $user = DB()->select("*", "cash_assistance as c, users as u", "c.user_id = u.id AND c.id = '$request[id]'")->get();

        $content = "We're Sorry to inform that your Requested Cash Assistance has been cancelled by the Brgy. Please go to the Brgy. Gym for more details. Thank you";

        $notif = [
            "user_id" => $auth,
            "request_id" => $request['id'],
            "notif_type" => "U",
            "notif_category" => "CA"
        ];

        DB()->insert("notif", $notif);

        $email = $this->sendEmail($user['email'], $user['fullname'], $content);

        echo $email;
    }

    public function store()
    {
        $request = Request::validate();
        $auth = Auth::user('id');
        $data = [
            "user_id" => $auth,
            "cash_amount" => $request['cash_amount'],
            "reason" => $request['request_reason'],
            "date_request" => $request['request_date'],
        ];

        $response = DB()->insert("cash_assistance", $data, "Y");

        $notif = [
            "user_id" => $auth,
            "request_id" => $response,
            "notif_type" => "SA",
            "notif_category" => "CA"
        ];

        DB()->insert("notif", $notif);

        echo $response;
    }

    public function sendEmail($email_address, $fullname, $content)
    {
        $subject = "Brgy. Tangub Emergency Assistance - Cash Assistance Request Status";

        $body = "<!DOCTYPE html>
                    <html>
                        <head>
                            <title></title>
                            <link rel='stylesheet' href='".public_url('/assets/adminty/bower_components/bootstrap/css/bootstrap.min.css')."'>
                        </head>
                        <body><h1>Hi ".ucwords($fullname).",</h1>
                    <p>".$content."</p>
                    
                    <p>Best Regards,<br />Management Team</p>
                    Please do not reply.
                        </body>
                    </html>";

        $recepients = $email_address;

        $sent = sendMail($subject, $body, $recepients);

        return $sent;
    }

    public function cashAssitanceList()
    {
        $id = Auth::user('id');

        $details = DB()->selectLoop("*", "cash_assistance", "user_id = '$id' ORDER BY date_added DESC")->get();

        $count = 1;
        $response['data'] = [];
        $list = [];
        foreach ($details as $detail) {
            $list = array();
            $list['id'] = $detail['id'];
            $list['count'] = $count++;
            $list['amount'] = "&#8369; ".number_format($detail['cash_amount'],2);
            $list['reason'] = $detail['reason'];
            $list['date_req'] = date("F d, Y", strtotime($detail['date_request']));
            $list['status'] = ($detail['status'] == 0) ? "<span style='color: orange'>Pending</span>" : (($detail['status'] == 1)?"<span style='color: green'>Approved</span>":"<span style='color: red'>Cancelled</span>");

            array_push($response['data'], $list);
        }

        echo json_encode($response);
    }

}
