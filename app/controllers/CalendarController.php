<?php

namespace App\Controllers;

use App\Core\Auth;
use App\Core\Request;

class CalendarController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Calendar Of Events";

        return view('/calendar/index', compact('pageTitle'));
    }

    public function store()
    {
        $request = Request::validate();

        $data = [
            "event_name" => $request['event_name'],
            "event_description" => $request['event_desc'],
            "event_date_from" => $request['event_date_from'],
            "event_date_to" => $request['event_date_to'],
            "created_by" => Auth::user('id')
        ];

        $response = DB()->insert("events", $data);

        echo $response;
    }

    public function drop()
    {
        $request = Request::validate('');
        $new_end = date("Y-m-d", strtotime("-1 day", strtotime($request['new_e'])));
        $update_data = [
            "event_date_from" => $request['new_s'],
            "event_date_to" => $new_end
        ];
        
        $response = DB()->update('events', $update_data, "id = '$request[id]'");

        echo $response;
    }

    public function delete()
    {
        $request = Request::validate('');

        $response = DB()->delete("events", "id = '$request[id]'");

        echo $response;
    }

    public function update()
    {
        $request = Request::validate('');

        $update_data = [
            "event_name" => $request['show_name'],
            "event_description" => $request['show_desc']
           
        ];
        
        $response = DB()->update('events', $update_data, "id = '$request[eventID]'");

        echo $response;
    }
}
