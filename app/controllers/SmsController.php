<?php

namespace App\Controllers;

use App\Core\Auth;
use App\Core\Request;

use SMSGatewayMe\Client\ApiClient;
use SMSGatewayMe\Client\Configuration;
use SMSGatewayMe\Client\Api\MessageApi;
use SMSGatewayMe\Client\Model\SendMessageRequest;

class SmsController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Sms";
        $msgs = DB()->selectLoop("*", "sms_header")->get();

        return view('/sms/index', compact('pageTitle', 'msgs'));
    }

    public function recepients()
    {
        $request = Request::validate('/sms', [
            "id" => ['required']
        ]);

        $msgs = DB()->selectLoop("CONCAT(m.firstname,' ',m.middlename,' ',m.lastname) as resident_fullname, h.id", "sms_header as h, sms_detail as d, resident_masterlist as m", "h.id = d.sms_id AND d.resident_id = m.id AND h.id = '$request[id]'")->get();

        $count = 1;
        $response['data'] = [];

        if (count($msgs) > 0) {
            $list = [];
            foreach ($msgs as $msg) {
                $list['sms_id'] = $msg['id'];
                $list['count'] = $count++;
                $list['fullname'] = $msg['resident_fullname'];

                array_push($response['data'], $list);
            }
        }
        echo json_encode($response);
    }

    public function send()
    {

        $request = Request::validate();

        $message = $request['sms_content'];
        $device_id = 127070;
        $current_time = date("Y-m-d H:i:s");

        $residents = DB()->selectLoop("*", "users", "contact != ''")->get();

        if (count($residents) < 1) {
            redirect('/sms', ["message" => "No contacts found in residents.", "status" => "danger"]);
        } else {
            $data = [];
            foreach ($residents as $resident) {
                $data = array(
                    "phone_number" => $resident['contact'],
                    "message" => $message,
                    "device_id" => $device_id
                );
                $sms = $this->sendSMS($data);
                if ($sms) {
                    $message = ["message" => "Oppss! There was an error encountered", "status" => "danger"];
                } else {

                    $header_data = [
                        "sms_content" => $message,
                        "device_id" => $device_id,
                        "sms_datetime" => $current_time
                    ];

                    $header_insert = DB()->insert("sms_header", $header_data, "Y");
                    if ($header_insert > 0) {

                        $detail_data = [
                            "sms_id" => $header_insert,
                            "resident_id" => $resident['id']
                        ];

                        $detail_insert = DB()->insert("sms_detail", $detail_data);
                    }

                   echo $sms;
               }
            }
        }
    }

    public function sendSMS($text)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://smsgateway.me/api/v4/message/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "[".json_encode($text)."]",
            CURLOPT_HTTPHEADER => array(
                "authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhZG1pbiIsImlhdCI6MTYzNDIwNDAzMywiZXhwIjo0MTAyNDQ0ODAwLCJ1aWQiOjg4NTkyLCJyb2xlcyI6WyJST0xFX1VTRVIiXX0.pmvs1PV48-WOaF8YqxGGlCSjhtqefzu0ccmdyjPd3w4",
                "cache-control: no-cache"
            ),
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        echo $err;
    }
}
