<?php

namespace App\Controllers;
use App\Core\App;
use App\Core\Auth;
use App\Core\Request;
class WelcomeController
{
    protected $pageTitle;

    public function home()
    {
        $pageTitle = "Home";

        return view('/home', compact('pageTitle'));
    }

    public function updateNotif()
    {
        $request = Request::validate();

        $data = [
            "status" => 1
        ];

        $response = DB()->update("notif", $data, "id = '$request[id]'");

        echo $response;
    }
}
