<?php

namespace App\Controllers;
use App\Core\Auth;
use App\Core\Request;

class ReportController
{
    protected $pageTitle;

    public function cashAssistance()
    {
        $pageTitle = "Monthly Cash Assistance";

        return view('/report/cash-assistance', compact('pageTitle'));
    }

    public function inventory()
    {
        $pageTitle = "Monthly Supply";

        $supply = DB()->selectLoop("*", "supplies")->get();

        return view('/report/inventory', compact('pageTitle', 'supply'));
    }

    public function inventorySupply()
    {
        $pageTitle = "Inventory AS OF";
       
        $supply = DB()->selectLoop("*", "`supply_stocks` as ss, `supplies` as s", "ss.supply_id = s.id GROUP BY supply_id, CONCAT(stock_measure,'',unit_measure) ORDER BY stock_measure DESC")->get();

        return view('/report/inventory-supply', compact('pageTitle', 'supply'));
    }

    public function supplyReport()
    {
        $request = Request::validate();

        $header['series'] = array();
        $data['name'] = "Months";
        $data['colorByPoint'] = true;
        $data['data'] = array();

        $months = array(
            "January" => 1,
            "February" => 2,
            "March" => 3,
            "April" => 4,
            "May" => 5,
            "June" => 6,
            "July" => 7,
            "August" => 8,
            "September" => 9,
            "October" => 10,
            "November" => 11,
            "December" => 12
        );

        foreach($months as $name => $number){
              
            $list = array();
            $list['name'] = $name; 

            $detail = DB()->select("SUM(d.quantity) as t","request as r, request_detail as d","MONTH(r.request_date) = '$number' AND r.status = 2 AND d.supply_id = '$request[supply_id]' AND r.id = d.header_id")->get();

            if($detail['t'] == 0 || $detail['t'] == null || $detail['t'] == ''){
                $total = 0;
            }else{	
                $total = $detail['t'] * 1;
            }

            $list['y'] = $total;
            array_push($data['data'], $list);
        }
        array_push($header['series'], $data);
		echo json_encode($header);
    }

    public function assistanceReport()
    {
        $header['series'] = array();
        $data['name'] = "Months";
        $data['colorByPoint'] = true;
        $data['data'] = array();

        $months = array(
            "January" => 1,
            "February" => 2,
            "March" => 3,
            "April" => 4,
            "May" => 5,
            "June" => 6,
            "July" => 7,
            "August" => 8,
            "September" => 9,
            "October" => 10,
            "November" => 11,
            "December" => 12
        );

        foreach($months as $name => $number){
              
            $list = array();
            $list['name'] = $name; 

            $detail = DB()->select("SUM(cash_amount) as t","cash_assistance","MONTH(date_request) = '$number' AND status = 1")->get();

            if($detail['t'] == 0 || $detail['t'] == null || $detail['t'] == ''){
                $total = 0;
            }else{	
                $total = $detail['t'] * 1;
            }

            $list['y'] = $total;
            array_push($data['data'], $list);
        }
        array_push($header['series'], $data);
		echo json_encode($header);
    }
}
