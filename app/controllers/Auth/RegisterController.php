<?php

namespace App\Controllers;

use App\Core\App;
use App\Core\Auth;
use App\Core\Request;

class RegisterController
{
    protected $pageTitle;

    public function index()
    {
        Auth::isAuthenticated();

        $purok = DB()->selectLoop("*", "purok_masterlist")->get();

        $pageTitle = "Register";
        return view('/auth/register', compact('pageTitle', 'purok'));
    }

    public function store()
    {
        $request = Request::validate('/register', [
            'email' => ['required', 'email'],
            'username' => ['required', 'unique:users'],
            'password' => ['required'],
        ]);

        $register_user = [
            'email' => $request['email'],
            'fullname' => $request['name'],
            'purok' => $request['purok'],
            'category' => $request['category'],
            'contact' => $request['contact'],
            'role_id' => 2,
            'username' => $request['username'],
            'password' => bcrypt($request['password']),
            'avatar' => "",
            'user_status' => 0,
            'reg_status' => 0,
            'updated_at' => date("Y-m-d H:i:s"),
            'created_at' => date("Y-m-d H:i:s")
        ];

        DB()->insert("users", $register_user);
        redirect('/register', ["message" => "Registration Submitted! <br> An Email will be sent to you regarding on your registration status.", "status" => "success"]);
    }
}
