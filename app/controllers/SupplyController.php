<?php

namespace App\Controllers;

use App\Core\Auth;
use App\Core\Request;

class SupplyController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Supply";
        // $supplTypes = DB()->selectLoop("*", "supply_type")
        //     ->withCount([
        //         "supplies" => ['id', 'supply_type']
        //     ])
        //     ->get();

        $supplTypes = DB()->selectLoop("*", "supply_type")->get();

        return view('/supply/index', compact('pageTitle', 'supplTypes'));
    }

    public function availability($type_id)
    {
        $pageTitle = "Supply Availability";
        $type_name = DB()->select("*", "supply_type", "id = '$type_id'")->get();
        $type = $type_name['type_name'];
        $supplies = DB()->selectLoop("*", "supplies", "supply_type = '$type_id'")->get();

        return view('/supply/availability', compact('pageTitle', 'type', 'type_id', 'supplies'));
    }

    public function stocks($supply)
    {
        $pageTitle = "Supply Inventory";
        $stocks = DB()->selectLoop("*", "supply_stocks", "supply_id = '$supply'")->get();
        $supplies = DB()->select("*", "supplies", "id = '$supply'")->get();
        $supply_name = $supplies['supply_name'];
        $measure = $supplies['measure'];
        $expiry = $supplies['expiry'];
        $type = $supplies['supply_type'];

        return view('/supply/stocks', compact('pageTitle', 'supply_name', 'stocks', 'measure', 'expiry', 'supply', 'type'));
    }

    public function addStocks()
    {
        $request = Request::validate();

        $data = [
            "supply_id" => $request['supply_id'],
            "unit_measure" => $request['res'],
            "quantity" => $request['quantity'],
            "stock_measure" => $request['unitofmeasure'],
            "expiry" => $request['expiryDate'],
            "description" => $request['description']
        ];

        $response = DB()->insert("supply_stocks", $data);

        echo $response;
    }
    public function savePerType()
    {
        $request = Request::validate('', [
            "supplyname" => ['required'],
            "supplytype" => ['required']
        ]);

        $measure = (isset($request['uom']))?1:0;
        $expiry = (isset($request['expDate']))?1:0;

        $form_to_supply = [
            "supply_name" => $request['supplyname'],
            "supply_type" => $request['supplytype'],
            "measure" => $measure,
            "expiry" => $expiry
        ];

        $response = DB()->insert("supplies", $form_to_supply);
        $message = ($response == 1) ? ["message" => "All Good! Supply Successfully Added", "status" => "success"] : ["message" => "Oppss! There was an error encountered", "status" => "danger"];

        redirect('/supply/availability/' . $request['supplytype'], $message);
    }

    public function store()
    {
        $request = Request::validate();

        

        $form_to_supply_category = [
            "type_name" => $request['category'],
        ];

        $response = DB()->insert("supply_type", $form_to_supply_category);
        $message = ($response == 1)
            ? ["message" => "All Good! Supply Category added successfully.", "status" => "success"]
            : ["message" => "Oppss! There was an error encountered.", "status" => "danger"];

        redirect('/supply/masterlist', $message);
    }

    public function stocksData()
    {
        $request = Request::validate();

        $stocks = DB()->selectLoop("*", "supply_stocks", "supply_id = '$request[supply]' ORDER BY date_added DESC")->get();

        $count = 1;
        $response['data'] = [];
        $list = [];
        foreach ($stocks as $stock) {
            $list = array();
            $list['stock'] = $stock['id'];
            $list['count'] = $count++;
            $list['unit_of_measure'] = ($stock['unit_measure'] == '')?"<span style='color: red'>N/A</span>":$stock['stock_measure']."".$stock['unit_measure'];
            $list['quantity'] = $stock['quantity'];
            $list['expiry'] = ($stock['expiry'] == '0000-00-00')?"<span style='color: red'>N/A</span>":date("F d, Y", strtotime($stock['expiry']));
            $list['date_added'] = date("F d, Y", strtotime($stock['date_added']));

            array_push($response['data'], $list);
        }

        echo json_encode($response);
    }

    public function deleteStocks()
    {
        $request = Request::validate();

        $delete_master = DB()->delete("supplies", "id = '$request[id]'");

        if($delete_master > 0){
            $delete_stocks = DB()->delete("supply_stocks", "supply_id = '$request[id]'");
            $delete_request = DB()->delete("request_detail", "supply_id = '$request[id]'");
        }

        echo $delete_master;

    }
}
