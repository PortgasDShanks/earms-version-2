<?php

namespace App\Controllers;

use App\Core\Request;
use App\Core\Auth;

class ResidentController
{
    protected $pageTitle;

    public function index()
    {
        $pageTitle = "Resident";

        $residents = DB()->selectLoop("*", "purok_masterlist")->get();
        $puroks = DB()->selectLoop("*", "purok_masterlist")->get();

        return view('/resident/index', compact('pageTitle', 'residents', 'puroks'));
    }

    public function readAllNotif($requests)
    {
        $ids = [];
        $type = getRole(Auth::user('role_id'));
        foreach ($requests as $request) {
            $ids[] = $request['id'];
        }

        $collectedIds = implode("','", $ids);
        DB()->delete('notif', "request_id in('$collectedIds') AND notif_type = '$type'");
    }

    public function store()
    {
        $request = Request::validate('/resident/masterlist', [
            "new_firstname" => ['required', 'unique:resident_masterlist,firstname', 'unique:users,username'],
            "new_lastname" => ['required', 'unique:resident_masterlist,lastname'],
            "purokList" => ['required'],
            "familyCat" => ['required']
        ]);

        $form_to_users = [
            "fullname" => $request['new_firstname'] . ' ' . $request['new_lastname'],
            "username" => $request['new_firstname'],
            "password" => bcrypt($request['new_lastname']),
            "role_id" => 2
        ];

        $last_user_id = DB()->insert("users", $form_to_users, "Y");

        $form_to_residents = [
            "firstname" => $request['new_firstname'],
            "lastname" => $request['new_lastname'],
            "purok" => $request['purokList'],
            "category" => $request['familyCat'],
            "user_id" => $last_user_id
        ];


        $response = DB()->insert("resident_masterlist", $form_to_residents);

        redirect('/resident/masterlist', ["message" => "Resident added successfully.", "status" => "success"]);
    }

    public function request()
    {
        $pageTitle = "Resident Request";
        $id = Auth::user('id');

        $requests = DB()->selectLoop("*", "request", "user_id = '$id' ORDER BY date_added DESC")->get();

        // $this->readAllNotif($requests);

        return view('/resident/request', compact('pageTitle', 'requests', 'id'));
    }

    public function residentPerPurok()
    {
        $request = Request::validate('/resident/masterlist', [
            "purokList" => ['required']
        ]);

        $residents = DB()->selectLoop("*", "users", "purok = '$request[purokList]' AND role_id = 2")->get();

        $response['data'] = [];
        if (count($residents) > 0) {
            $count = 1;
            $list = [];
            foreach ($residents as $resident) {
                $list['resident_id'] = $resident['id'];
                $list['count'] = $count++;
                $list['fullname'] = $resident['fullname'];
                $list['category'] = ($resident['category'] == 'H')?"Head":"Member";
                $list['email'] = $resident['email'];
                $list['contact'] = "+63".ltrim($resident['contact'], '0');
                $list['status'] = ($resident['reg_status'] == 0) ? "<span style='color: orange'>Pending</span>" : "<span style='color: green'>Approved</span>";

                $display = ($resident['reg_status'] == 1)?"display:none":"";

                $list['action'] = "<i data-toggle='tooltip' data-placement='bottom' title='Approve Registration' style='cursor:pointer;background-color: #0cb4c9;padding: 5px;border-radius: 17px;".$display."' class='material-icons text-white btn-round' onclick='approveRegister(".$resident['id'].")' id='updateBtn".$resident['id']."'>thumb_up</i>";

           

                array_push($response['data'], $list);
            }
        }

        echo json_encode($response);
    }

    public function approve()
    {
        $request = Request::validate();

        $response = DB()->update("users", ["reg_status" => 1], "id = '$request[id]'");
        if($response > 0){
            $user = DB()->select("*","users"," id = '$request[id]'")->get();

            $send = $this->sendEmail($user['email'], $user['fullname']);
        }

        echo $send;
    }

    public function sendEmail($email_address, $fullname)
    {
        $subject = "Brgy. Tangub Emergency Assistance - Account Status";

        $body = "<!DOCTYPE html>
                    <html>
                        <head>
                            <title></title>
                            <link rel='stylesheet' href='".public_url('/assets/adminty/bower_components/bootstrap/css/bootstrap.min.css')."'>
                        </head>
                        <body><h1>Hi ".ucwords($fullname).",</h1>
                    <p>Your Account has been approved by the brgy. You can now use our application. Thank you</p>
                    
                    <p>Best Regards,<br />Management Team</p>
                    Please do not reply.
                        </body>
                    </html>";

        $recepients = $email_address;

        $sent = sendMail($subject, $body, $recepients);

        return $sent;
    }

    public function viewRequest($id)
    {
        $pageTitle = "View Request Details";

        $requests = DB()->selectLoop("*", "request as r, users as u", "r.user_id = u.id AND r.id = '$id'")->get();

        $supplyTypes = DB()->selectLoop("*", "supply_type")->get();
        
       
        return view('/request/view-by-user', compact('pageTitle', 'requests', 'id', 'supplyTypes'));
    }
}
