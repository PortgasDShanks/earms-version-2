-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2021 at 12:51 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `earms_sprnva`
--

-- --------------------------------------------------------

--
-- Table structure for table `cash_assistance`
--

CREATE TABLE `cash_assistance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cash_amount` decimal(12,3) NOT NULL,
  `reason` text NOT NULL,
  `date_request` date NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cash_assistance`
--

INSERT INTO `cash_assistance` (`id`, `user_id`, `cash_amount`, `reason`, `date_request`, `date_added`, `status`) VALUES
(1, 6, '1000.000', 'pang semento dalan sa roadside', '2021-11-15', '2021-11-12 11:18:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `event_name` text NOT NULL,
  `event_description` text NOT NULL,
  `event_date_from` datetime NOT NULL,
  `event_date_to` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `event_description`, `event_date_from`, `event_date_to`, `created_by`) VALUES
(2, 'test test', 'TEst desc', '2021-11-29 00:00:00', '2021-11-30 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(11) NOT NULL,
  `migrations` text NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migrations`, `batch`) VALUES
(1, '20210408051901_create_password_resets_table', 1),
(2, '20210408051901_create_roles_table', 1),
(3, '20210408051901_create_users_table', 1),
(4, '20210811092403_create_notif_table', 1),
(5, '20210811092655_create_purok_masterlist', 1),
(6, '20210811092809_create_request_table', 1),
(7, '20210811093140_create_request_detail_table', 1),
(8, '20210811093344_create_resident_masterlist_table', 1),
(9, '20210811093603_create_sms_detail_table', 1),
(10, '20210811093724_create_sms_header_table', 1),
(11, '20210811093839_create_supplies_table', 1),
(12, '20210811094007_create_supply_type_table', 1),
(13, '20210811094338_alter_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notif`
--

CREATE TABLE `notif` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` int(1) NOT NULL,
  `notif_type` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notif`
--

INSERT INTO `notif` (`id`, `user_id`, `request_id`, `date_added`, `status`, `notif_type`) VALUES
(1, 6, 5, '2021-10-14 05:35:09', 0, 'U'),
(2, 6, 4, '2021-10-14 06:39:20', 0, 'SA'),
(3, 6, 3, '2021-10-14 06:40:54', 0, 'SA'),
(4, 6, 5, '2021-10-14 22:10:42', 0, 'SA'),
(5, 4, 25, '2021-10-14 23:39:47', 0, 'U'),
(6, 4, 25, '2021-10-14 23:39:58', 0, 'SA'),
(7, 4, 25, '2021-10-14 23:42:51', 0, 'SA'),
(8, 4, 25, '2021-10-22 17:38:00', 0, 'SA');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purok_masterlist`
--

CREATE TABLE `purok_masterlist` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purok_masterlist`
--

INSERT INTO `purok_masterlist` (`id`, `name`) VALUES
(2, 'Roadside'),
(3, 'Greenfield');

-- --------------------------------------------------------

--
-- Table structure for table `request`
--

CREATE TABLE `request` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `request_date` datetime NOT NULL,
  `other_request` varchar(100) NOT NULL,
  `request_specify` text NOT NULL,
  `remarks` text NOT NULL,
  `date_added` datetime NOT NULL,
  `status` int(1) NOT NULL COMMENT '0=requested , 1=approved, 2=completed, 3=cancelled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request`
--

INSERT INTO `request` (`id`, `user_id`, `request_date`, `other_request`, `request_specify`, `remarks`, `date_added`, `status`) VALUES
(1, 6, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 04:03:11', 0),
(2, 6, '2021-10-16 00:00:00', '', '', 'test', '2021-10-14 04:24:57', 0),
(3, 6, '2021-10-16 00:00:00', '', '', 'test', '2021-10-14 04:25:59', 2),
(4, 6, '2021-10-18 00:00:00', '', '', 'test', '2021-10-14 04:28:38', 2),
(5, 6, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 04:29:15', 3),
(6, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 22:32:29', 0),
(7, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 22:37:45', 0),
(8, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 22:43:42', 0),
(9, 4, '2021-10-15 00:00:00', '', '', 'tedxt', '2021-10-14 22:48:24', 0),
(10, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 22:48:58', 0),
(11, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 22:49:44', 0),
(12, 4, '2021-10-16 00:00:00', '', '', 'test', '2021-10-14 22:50:22', 0),
(13, 4, '2021-10-15 00:00:00', '', '', 'vbvbfgg', '2021-10-14 22:52:27', 0),
(14, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 22:53:17', 0),
(15, 4, '2021-10-16 00:00:00', '', '', 'sadasdas', '2021-10-14 22:54:36', 0),
(16, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 22:55:13', 0),
(17, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 22:58:55', 0),
(18, 4, '2021-10-15 00:00:00', '', '', 'ghhnbbvgfgh', '2021-10-14 23:00:27', 0),
(19, 4, '2021-10-15 00:00:00', '', '', 'fgfgg', '2021-10-14 23:01:02', 0),
(20, 4, '2021-10-15 00:00:00', '', '', 'asdasdas', '2021-10-14 23:02:13', 0),
(21, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 23:07:32', 0),
(22, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 23:09:25', 0),
(23, 4, '2021-10-15 00:00:00', '', '', 'asdasd', '2021-10-14 23:10:10', 0),
(24, 4, '2021-10-15 00:00:00', '', '', 'test', '2021-10-14 23:11:54', 0),
(25, 4, '2021-10-15 00:00:00', '', '', 'sdasdas', '2021-10-14 23:28:10', 2);

-- --------------------------------------------------------

--
-- Table structure for table `request_detail`
--

CREATE TABLE `request_detail` (
  `id` int(11) NOT NULL,
  `header_id` int(11) NOT NULL,
  `supply_id` int(11) NOT NULL,
  `quantity` decimal(12,3) NOT NULL,
  `category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_detail`
--

INSERT INTO `request_detail` (`id`, `header_id`, `supply_id`, `quantity`, `category`) VALUES
(1, 1, 2, '5.000', '1'),
(2, 2, 2, '5.000', '1'),
(3, 2, 3, '5.000', '1'),
(4, 2, 2, '3.000', '1'),
(5, 3, 2, '1.000', '1'),
(6, 4, 2, '1.000', '1'),
(9, 5, 2, '5.000', '1'),
(10, 25, 2, '20.000', '1');

-- --------------------------------------------------------

--
-- Table structure for table `resident_masterlist`
--

CREATE TABLE `resident_masterlist` (
  `id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `purok` int(11) NOT NULL,
  `category` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) UNSIGNED NOT NULL,
  `role` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`) VALUES
(1, 'SA', NULL),
(2, 'U', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sms_detail`
--

CREATE TABLE `sms_detail` (
  `id` int(11) NOT NULL,
  `sms_id` int(11) NOT NULL,
  `resident_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_detail`
--

INSERT INTO `sms_detail` (`id`, `sms_id`, `resident_id`) VALUES
(1, 1, 3),
(2, 2, 3),
(3, 3, 3),
(4, 4, 3),
(5, 5, 3),
(6, 6, 3),
(7, 7, 4),
(8, 8, 6),
(9, 9, 3),
(10, 10, 4),
(11, 11, 6),
(12, 12, 3),
(13, 13, 4),
(14, 14, 6),
(15, 15, 3),
(16, 16, 4),
(17, 17, 6),
(18, 18, 3),
(19, 19, 4),
(20, 20, 6),
(21, 21, 3),
(22, 22, 4),
(23, 23, 6),
(24, 24, 3),
(25, 25, 4),
(26, 26, 6),
(27, 27, 3),
(28, 28, 4),
(29, 29, 6),
(30, 30, 3),
(31, 31, 4),
(32, 32, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sms_header`
--

CREATE TABLE `sms_header` (
  `id` int(11) NOT NULL,
  `sms_content` text NOT NULL,
  `device_id` int(11) NOT NULL,
  `sms_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sms_header`
--

INSERT INTO `sms_header` (`id`, `sms_content`, `device_id`, `sms_datetime`) VALUES
(1, 'Test SMS', 124389, '2021-10-14 17:28:23'),
(2, 'TEST SMS FOR API', 124389, '2021-10-14 17:33:03'),
(3, 'TEST SMS API', 124389, '2021-10-14 17:34:19'),
(4, 'SMS', 124389, '2021-10-14 17:35:09'),
(5, 'TEST', 124389, '2021-10-14 18:12:50'),
(6, 'sdasd asdasd asdasds', 124389, '2021-10-14 18:48:12'),
(7, 'Array', 124389, '2021-10-14 18:48:12'),
(8, 'Array', 124389, '2021-10-14 18:48:12'),
(9, 'erwrwe rwe rwe rwerwe', 124389, '2021-10-14 18:49:05'),
(10, 'Array', 124389, '2021-10-14 18:49:05'),
(11, 'Array', 124389, '2021-10-14 18:49:05'),
(12, 'ertreterterter', 124389, '2021-10-14 18:50:18'),
(13, 'Array', 124389, '2021-10-14 18:50:18'),
(14, 'Array', 124389, '2021-10-14 18:50:18'),
(15, 'TEST SMS FOR SMSGATEWAY API', 124389, '2021-10-14 18:55:45'),
(16, 'Array', 124389, '2021-10-14 18:55:45'),
(17, 'Array', 124389, '2021-10-14 18:55:45'),
(18, 'TESST SMS', 124389, '2021-10-14 19:38:31'),
(19, 'Array', 124389, '2021-10-14 19:38:31'),
(20, 'Array', 124389, '2021-10-14 19:38:31'),
(21, 'wyyyyyy', 124389, '2021-10-14 19:40:06'),
(22, 'Array', 124389, '2021-10-14 19:40:06'),
(23, 'Array', 124389, '2021-10-14 19:40:06'),
(24, 'sms', 124389, '2021-10-14 19:43:21'),
(25, 'Array', 124389, '2021-10-14 19:43:21'),
(26, 'Array', 124389, '2021-10-14 19:43:21'),
(27, 'test sms', 124389, '2021-10-14 19:44:50'),
(28, 'Array', 124389, '2021-10-14 19:44:50'),
(29, 'Array', 124389, '2021-10-14 19:44:50'),
(30, 'SMS API', 124389, '2021-10-14 20:13:14'),
(31, 'SMS API', 124389, '2021-10-14 20:13:14'),
(32, 'SMS API', 124389, '2021-10-14 20:13:14');

-- --------------------------------------------------------

--
-- Table structure for table `supplies`
--

CREATE TABLE `supplies` (
  `id` int(11) NOT NULL,
  `supply_name` varchar(50) NOT NULL,
  `supply_type` varchar(20) NOT NULL,
  `measure` int(1) NOT NULL,
  `expiry` int(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT current_timestamp(),
  `supply_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplies`
--

INSERT INTO `supplies` (`id`, `supply_name`, `supply_type`, `measure`, `expiry`, `date_added`, `supply_status`) VALUES
(1, 'test', '1', 0, 0, '0000-00-00 00:00:00', 0),
(2, 'Biogesic', '1', 0, 1, '2021-10-13 20:59:33', 0),
(3, 'Paracetamol', '1', 1, 1, '2021-10-13 22:52:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `supply_stocks`
--

CREATE TABLE `supply_stocks` (
  `id` int(11) NOT NULL,
  `supply_id` int(11) NOT NULL,
  `unit_measure` varchar(20) NOT NULL,
  `stock_measure` int(11) NOT NULL,
  `expiry` date NOT NULL,
  `description` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `supply_stocks`
--

INSERT INTO `supply_stocks` (`id`, `supply_id`, `unit_measure`, `stock_measure`, `expiry`, `description`, `quantity`, `date_added`) VALUES
(6, 3, 'mg', 500, '2021-10-15', 'test', 0, '2021-10-14 01:23:33'),
(7, 3, 'mg', 500, '2021-10-13', 'test', 20, '2021-10-14 01:24:38'),
(8, 3, 'mg', 250, '2021-10-13', 'test', 10, '2021-10-14 03:07:07'),
(9, 3, 'ml', 1000, '2021-10-13', 'test', 5, '2021-10-14 03:07:57'),
(10, 2, '', 0, '2021-10-18', 'sample', 50, '2021-10-14 16:13:17'),
(11, 2, '', 0, '2021-10-19', 'text', 50, '2021-10-14 16:13:56'),
(12, 2, '', 0, '2021-10-20', 'test', 50, '2021-10-14 16:15:50'),
(13, 2, '', 0, '2021-10-15', 'oooooo', 20, '2021-10-14 16:26:18'),
(14, 1, '', 0, '0000-00-00', 'dsffsdfsd', 50, '2021-10-14 17:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `supply_type`
--

CREATE TABLE `supply_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supply_type`
--

INSERT INTO `supply_type` (`id`, `type_name`) VALUES
(1, 'Medicines'),
(2, 'necessities'),
(3, 'Others'),
(4, 'Items'),
(5, 'Cash Assistance');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `fullname` varchar(200) DEFAULT NULL,
  `purok` int(11) DEFAULT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `category` varchar(5) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `password` text NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `avatar` varchar(100) NOT NULL,
  `user_status` int(1) NOT NULL COMMENT '0=logged out, 1=logged in, 2=AFK, 3=Snooze',
  `reg_status` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `fullname`, `purok`, `contact`, `category`, `username`, `password`, `role_id`, `remember_token`, `updated_at`, `created_at`, `avatar`, `user_status`, `reg_status`) VALUES
(1, 'ibayonabel@gmail.com', 'Abel Bayon', NULL, NULL, NULL, 'abel', '$2y$10$cL74b5fpIDNPjoXCjwVsY..Zs6CRgwHc2ui82CpnZwa6NyG.FVGy6', 1, NULL, '2021-08-13 22:36:12', '2021-08-13 22:36:12', '', 0, 0),
(2, 'dams2020@gmail.com', 'Mina Myoui', NULL, NULL, NULL, 'mina', '$2y$10$AZFNdSzFmag1VkPH40e9T.4KF86Ni.mga0F2LPReAg3dEg.rIFvJu', 1, NULL, '2021-09-22 17:36:01', '2021-09-22 17:36:01', '', 0, 0),
(3, 'abelfullbusterbayon@gmail.com', 'Kim Heechul', 2, '09213807764', 'H', 'kim', '$2y$10$HHx//hxi2jsFFHrH49IfTOMN6qHVB.wnUpXCu5ynlmgeYHLZr0TP.', 2, NULL, '2021-10-13 05:29:25', '2021-10-13 05:29:25', '', 0, 0),
(4, 'markyzerortega0319@gmail.com', 'Lee Kwangsoo', 3, '09454564469', 'H', 'lee', '$2y$10$Y1OBE7.AvY0hGwPXt0E5Vej35pNBB75RE0t4ei8SfZoiDI12q1btO', 2, NULL, '2021-10-13 05:31:26', '2021-10-13 05:31:26', '', 0, 1),
(6, 'ibayonabel@gmail.com', 'Steve Bayon', 2, '09701994033', 'H', 'steve', '$2y$10$bafKnO4fjy9xvqz5rLiUveg4SAA3xly7GG0d0GQoF.VjWBlwwhi0O', 2, NULL, '2021-10-14 03:59:58', '2021-10-14 03:59:58', '', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cash_assistance`
--
ALTER TABLE `cash_assistance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `notif`
--
ALTER TABLE `notif`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`email`) USING BTREE;

--
-- Indexes for table `purok_masterlist`
--
ALTER TABLE `purok_masterlist`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `request`
--
ALTER TABLE `request`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `request_detail`
--
ALTER TABLE `request_detail`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `resident_masterlist`
--
ALTER TABLE `resident_masterlist`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `sms_detail`
--
ALTER TABLE `sms_detail`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `sms_header`
--
ALTER TABLE `sms_header`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `supplies`
--
ALTER TABLE `supplies`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `supply_stocks`
--
ALTER TABLE `supply_stocks`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `supply_type`
--
ALTER TABLE `supply_type`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cash_assistance`
--
ALTER TABLE `cash_assistance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `notif`
--
ALTER TABLE `notif`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `purok_masterlist`
--
ALTER TABLE `purok_masterlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `request`
--
ALTER TABLE `request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `request_detail`
--
ALTER TABLE `request_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `resident_masterlist`
--
ALTER TABLE `resident_masterlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sms_detail`
--
ALTER TABLE `sms_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `sms_header`
--
ALTER TABLE `sms_header`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `supplies`
--
ALTER TABLE `supplies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `supply_stocks`
--
ALTER TABLE `supply_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `supply_type`
--
ALTER TABLE `supply_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
